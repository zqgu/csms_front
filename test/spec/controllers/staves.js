'use strict';

describe('Controller: StavesCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var StavesCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StavesCtrl = $controller('StavesCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
