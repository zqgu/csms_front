'use strict';

describe('Controller: StaveCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var StaveCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StaveCtrl = $controller('StaveCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
