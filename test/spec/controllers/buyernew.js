'use strict';

describe('Controller: BuyernewCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var BuyernewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BuyernewCtrl = $controller('BuyernewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
