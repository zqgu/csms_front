'use strict';

describe('Controller: TravelapprovalCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var TravelapprovalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TravelapprovalCtrl = $controller('TravelapprovalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
