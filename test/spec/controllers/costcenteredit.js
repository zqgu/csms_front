'use strict';

describe('Controller: CostcentereditCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var CostcentereditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CostcentereditCtrl = $controller('CostcentereditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
