'use strict';

describe('Controller: EmployeegradeeditCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var EmployeegradeeditCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EmployeegradeeditCtrl = $controller('EmployeegradeeditCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
