'use strict';

describe('Controller: TravelapplyCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var TravelapplyCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TravelapplyCtrl = $controller('TravelapplyCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
