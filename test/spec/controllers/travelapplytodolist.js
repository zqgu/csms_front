'use strict';

describe('Controller: TravelapplytodolistCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var TravelapplytodolistCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TravelapplytodolistCtrl = $controller('TravelapplytodolistCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
