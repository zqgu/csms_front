'use strict';

describe('Controller: EnterpriseCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var EnterpriseCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EnterpriseCtrl = $controller('EnterpriseCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
