'use strict';

describe('Controller: CostcenternewCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var CostcenternewCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CostcenternewCtrl = $controller('CostcenternewCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
