'use strict';

describe('Controller: EnterpriseregisterCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var EnterpriseregisterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    EnterpriseregisterCtrl = $controller('EnterpriseregisterCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
