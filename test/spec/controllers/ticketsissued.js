'use strict';

describe('Controller: TicketsissuedCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var TicketsissuedCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TicketsissuedCtrl = $controller('TicketsissuedCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
