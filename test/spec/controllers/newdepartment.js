'use strict';

describe('Controller: NewdepartmentCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var NewdepartmentCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewdepartmentCtrl = $controller('NewdepartmentCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
