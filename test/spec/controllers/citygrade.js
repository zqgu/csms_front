'use strict';

describe('Controller: CitygradeCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var CitygradeCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CitygradeCtrl = $controller('CitygradeCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
