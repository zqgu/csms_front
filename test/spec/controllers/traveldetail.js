'use strict';

describe('Controller: TraveldetailCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var TraveldetailCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    TraveldetailCtrl = $controller('TraveldetailCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
