'use strict';

describe('Controller: RoleusersCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var RoleusersCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RoleusersCtrl = $controller('RoleusersCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
