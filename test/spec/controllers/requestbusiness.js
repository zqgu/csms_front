'use strict';

describe('Controller: RequestbusinessCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var RequestbusinessCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RequestbusinessCtrl = $controller('RequestbusinessCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
