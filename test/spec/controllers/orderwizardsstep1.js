'use strict';

describe('Controller: Orderwizardsstep1Ctrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var Orderwizardsstep1Ctrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    Orderwizardsstep1Ctrl = $controller('Orderwizardsstep1Ctrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
