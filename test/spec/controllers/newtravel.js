'use strict';

describe('Controller: NewtravelCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var NewtravelCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewtravelCtrl = $controller('NewtravelCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
