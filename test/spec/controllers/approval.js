'use strict';

describe('Controller: ApprovalCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var ApprovalCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ApprovalCtrl = $controller('ApprovalCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
