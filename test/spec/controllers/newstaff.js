'use strict';

describe('Controller: NewstaffCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var NewstaffCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    NewstaffCtrl = $controller('NewstaffCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
