'use strict';

describe('Controller: BookflightCtrl', function () {

  // load the controller's module
  beforeEach(module('projApp'));

  var BookflightCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BookflightCtrl = $controller('BookflightCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
