'use strict';

describe('Filter: bzDiscount', function () {

  // load the filter's module
  beforeEach(module('projApp'));

  // initialize a new instance of the filter before each test
  var bzDiscount;
  beforeEach(inject(function ($filter) {
    bzDiscount = $filter('bzDiscount');
  }));

  it('should return the input prefixed with "bzDiscount filter:"', function () {
    var text = 'angularjs';
    expect(bzDiscount(text)).toBe('bzDiscount filter: ' + text);
  });

});
