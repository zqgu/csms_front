'use strict';

describe('Filter: sortFilter', function () {

  // load the filter's module
  beforeEach(module('projApp'));

  // initialize a new instance of the filter before each test
  var sortFilter;
  beforeEach(inject(function ($filter) {
    sortFilter = $filter('sortFilter');
  }));

  it('should return the input prefixed with "sortFilter filter:"', function () {
    var text = 'angularjs';
    expect(sortFilter(text)).toBe('sortFilter filter: ' + text);
  });

});
