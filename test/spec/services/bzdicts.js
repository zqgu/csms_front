'use strict';

describe('Service: bzDicts', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var bzDicts;
  beforeEach(inject(function (_bzDicts_) {
    bzDicts = _bzDicts_;
  }));

  it('should do something', function () {
    expect(!!bzDicts).toBe(true);
  });

});
