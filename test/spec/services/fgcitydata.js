'use strict';

describe('Service: fgCityData', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var fgCityData;
  beforeEach(inject(function (_fgCityData_) {
    fgCityData = _fgCityData_;
  }));

  it('should do something', function () {
    expect(!!fgCityData).toBe(true);
  });

});
