'use strict';

describe('Service: fgAuthenticate', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var fgAuthenticate;
  beforeEach(inject(function (_fgAuthenticate_) {
    fgAuthenticate = _fgAuthenticate_;
  }));

  it('should do something', function () {
    expect(!!fgAuthenticate).toBe(true);
  });

});
