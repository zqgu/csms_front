'use strict';

describe('Service: fgDictData', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var fgDictData;
  beforeEach(inject(function (_fgDictData_) {
    fgDictData = _fgDictData_;
  }));

  it('should do something', function () {
    expect(!!fgDictData).toBe(true);
  });

});
