'use strict';

describe('Service: bzMessagesService', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var bzMessagesService;
  beforeEach(inject(function (_bzMessagesService_) {
    bzMessagesService = _bzMessagesService_;
  }));

  it('should do something', function () {
    expect(!!bzMessagesService).toBe(true);
  });

});
