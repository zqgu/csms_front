'use strict';

describe('Service: bzFlQuery', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var bzFlQuery;
  beforeEach(inject(function (_bzFlQuery_) {
    bzFlQuery = _bzFlQuery_;
  }));

  it('should do something', function () {
    expect(!!bzFlQuery).toBe(true);
  });

});
