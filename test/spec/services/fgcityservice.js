'use strict';

describe('Service: fgCityService', function () {

  // load the service's module
  beforeEach(module('projApp'));

  // instantiate service
  var fgCityService;
  beforeEach(inject(function (_fgCityService_) {
    fgCityService = _fgCityService_;
  }));

  it('should do something', function () {
    expect(!!fgCityService).toBe(true);
  });

});
