'use strict';

describe('Directive: fgSiderNav', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fg-sider-nav></fg-sider-nav>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the fgSiderNav directive');
  }));
});
