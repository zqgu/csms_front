'use strict';

describe('Directive: fgFormControl', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fg-form-control></fg-form-control>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the fgFormControl directive');
  }));
});
