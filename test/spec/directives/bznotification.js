'use strict';

describe('Directive: bzNotification', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bz-notification></bz-notification>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bzNotification directive');
  }));
});
