'use strict';

describe('Directive: bzStavesSelector', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bz-staves-selector></bz-staves-selector>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bzStavesSelector directive');
  }));
});
