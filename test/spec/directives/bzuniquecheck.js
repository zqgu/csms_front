'use strict';

describe('Directive: bzUniqueCheck', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bz-unique-check></bz-unique-check>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bzUniqueCheck directive');
  }));
});
