'use strict';

describe('Directive: fgSimpleTree', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fg-simple-tree></fg-simple-tree>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the fgSimpleTree directive');
  }));
});
