'use strict';

describe('Directive: bzAirTicketRequest', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bz-air-ticket-request></bz-air-ticket-request>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bzAirTicketRequest directive');
  }));
});
