'use strict';

describe('Directive: bzMainPage', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bz-main-page></bz-main-page>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bzMainPage directive');
  }));
});
