'use strict';

describe('Directive: bzPersonalCenter', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<bz-personal-center></bz-personal-center>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the bzPersonalCenter directive');
  }));
});
