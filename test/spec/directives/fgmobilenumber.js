'use strict';

describe('Directive: fgMobileNumber', function () {

  // load the directive's module
  beforeEach(module('projApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<fg-mobile-number></fg-mobile-number>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the fgMobileNumber directive');
  }));
});
