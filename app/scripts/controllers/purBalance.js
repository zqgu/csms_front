'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:Orderwizardsstep1Ctrl
 * @description
 * # Orderwizardsstep1Ctrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('PurBalanceCtrl', function ($scope, $http, $modal, bzDicts) {

    var tmpArr1 = [];//应收选中
    var tmpArr2 = [];//应付选中

    $scope.purchaser = {};
    $scope.queryParams = {};
    $scope.collectMoney = 0;//应收金额
    $scope.payMoney = 0;//应付金额


    /* 采购商财务明细的操作类型 */
    bzDicts.query('FIN_PUR_DETAIL_TYPE')
      .then(function(data){
        $scope.purDetialTypes = data;
      });

    /* 查询采购商 */
    $http.get('api/purchasers')
      .success(function(data){
        $scope.purchasers = data.results;
      });

    /* 应收查询 */
    $scope.queryCollect = function(){
      $http.get('api/settlements/collect', {params: $scope.queryParams})
        .success(function(data){
          $scope.collects = data.results;
        });
    };

    /* 应付查询 */
    $scope.queryPay = function(){
      $http.get('api/settlements/pay', {params: $scope.queryParams})
        .success(function(data){
          $scope.pays = data.results;
        });
    };

    /* 采购商结算 */
    $scope.balance = function(flag){
      if(flag === 1){
        if(tmpArr1.length == 0){
          return alert('请选择应收款信息！');
        }
      }else if(flag === 2){
        if(tmpArr2.length == 0){
          return alert('请选择应付款信息！');
        }
      }else if(flag === 3){
        if(tmpArr1.length == 0 && tmpArr2.length == 0){
          return alert('请选择账款信息！');
        }
      }

      var orderids = [];
      var refundIds = [];
      var changeIds = [];
      var detailIds = [];

      _.each(tmpArr1, function(v){
        orderids.push(v.order.planeOrderId);
        detailIds.push(v.detailId);
      });

      _.each(tmpArr2, function(v){
        refundIds.push(v.refund.refundId);
        detailIds.push(v.detailId);
      });

      var modalInstance = $modal.open({//打开对话框
        templateUrl: 'views/purBalanceModal.html',
        controller: 'PurBalanceModalCtrl',
        resolve: {
          item: function(){
            return {
              payObjectType: 1,
              payObjectId: $scope.purchaser.purchaserId,
              prepayment: $scope.purchaser.prepayment,
              collectMoney: (flag == 1 || flag == 3)? $scope.collectMoney : 0,
              payMoney: (flag == 2 || flag == 3)? $scope.payMoney : 0,
              orderIds: (flag == 1 || flag == 3)? orderids : [],
              refundIds: (flag == 2 || flag == 3)? refundIds : [],
              changeIds: [],
              detailIds: detailIds
            }
          }
        }
      });

      modalInstance.result.then(function (flag) {//信息回传
        if(flag){
          $scope.checkboxes1.checked = false;
          $scope.checkboxes2.checked = false;
          _.each($scope.collects, function (item) {
            $scope.checkboxes1.items[item.detailId] = false;
          });
          _.each($scope.pays, function (item) {
            $scope.checkboxes2.items[item.detailId] = false;
          });
          $scope.query();
        }
      });
    };

    /* 查询 */
    $scope.query = function(){
      if(!$scope.purchaser.purchaserId){
        return alert('请选择采购商');
      }
      $scope.queryParams.purchaserId = $scope.purchaser.purchaserId;
      $scope.queryParams.startTime = $scope.startTime;
      $scope.queryParams.endTime = $scope.endTime;
      $scope.queryCollect();
      $scope.queryPay();
    };

    $scope.checkboxes1 = {checked: false, items: {}};
    $scope.checkboxes2 = {checked: false, items: {}};

    /*监听全选*/
    $scope.$watch('checkboxes1.checked', function (value) {
      _.each($scope.collects, function (item) {
        $scope.checkboxes1.items[item.detailId] = value;
      });
    });

    /*监听全选*/
    $scope.$watch('checkboxes2.checked', function (value) {
      _.each($scope.pays, function (item) {
        $scope.checkboxes2.items[item.detailId] = value;
      });
    });

    /*监听结算选择*/
    $scope.$watch('checkboxes1.items', function () {
      if (!$scope.collects) {
        return;
      }
      var
        checked = 0,
        unchecked = 0,
        isChecked,
        total = $scope.collects.length;

      tmpArr1 = [],//当前选中的checkbox

      _.each($scope.collects, function (item) {
        isChecked = $scope.checkboxes1.items[item.detailId];
        if(isChecked){
          tmpArr1.push(item);
        }
        checked += isChecked || 0;
        unchecked += !isChecked || 0;
      });

      $scope.collectMoney = 0;
      _.each(tmpArr1, function(v){
        $scope.collectMoney += v.operMoney;
      });

      if ((unchecked === 0) || (checked === 0)) {
        $scope.checkboxes1.checked = (checked === total);
      }

      $('#select_all1').prop('indeterminate', (checked !== 0 && unchecked !== 0));//是否半选
    }, true);

    /*监听结算选择*/
    $scope.$watch('checkboxes2.items', function () {
      if (!$scope.pays) {
        return;
      }
      var
        checked = 0,
        unchecked = 0,
        isChecked,
        total = $scope.pays.length;

      tmpArr2 = [],//当前选中的checkbox

      _.each($scope.pays, function (item) {
        isChecked = $scope.checkboxes2.items[item.detailId];
        if(isChecked){
          tmpArr2.push(item);
        }
        checked += isChecked || 0;
        unchecked += !isChecked || 0;
      });

      $scope.payMoney = 0;
      _.each(tmpArr2, function(v){
        $scope.payMoney += v.operMoney;
      });

      if ((unchecked === 0) || (checked === 0)) {
        $scope.checkboxes2.checked = (checked === total);
      }

      $('#select_all2').prop('indeterminate', (checked !== 0 && unchecked !== 0));//是否半选
    }, true);
  });
