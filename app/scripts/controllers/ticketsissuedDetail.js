'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsissuedDetailCtrl', function ($scope, $http, $modal, $routeParams, bzIssue, $route, bzDicts) {

    var issueId = $routeParams.id;

    $scope.UNISSUE = '2';//未出票
    $scope.ISSUEING = '3';//出票中
    $scope.ISSUED = '4';//已出票
    $scope.ISSUE_FAIL = '5';//已出票

    $scope.state = bzIssue.getState();//当前出票状态

    /* 查询某出票订单 */
    function query() {
      $http.get('api/planeOrders/' + issueId)
        .success(function(data) {
          $scope.record = data.results;
        });
    }

    query();

    bzDicts.query('PAY_TYPE')//支付方式
      .then(function(data){
        $scope.payTypes = data;
      });

    /* 开始出票 */
    $scope.startIssue = function(){
      $http.put('api/planeOrders/' + $scope.record.planeOrderId + '/outTicket', {})
        .success(function(){
          bzIssue.setState($scope.ISSUEING);
          $route.reload();
        })
    };

    /* 完成出票 */
    $scope.finishIssue = function() {
      $http.put('api/planeOrders/' + $scope.record.planeOrderId + '/backfill', $scope.record)
        .success(function(){
          bzIssue.setState($scope.ISSUED);
          $route.reload();
        });
    };

    /* 出票失败 */
    $scope.failIssue = function() {
      $scope.record.state = $scope.ISSUE_FAIL;
      $http.put('api/planeOrders/' + $scope.record.planeOrderId + '/backfill', $scope.record)
        .success(function(){
          bzIssue.setState($scope.ISSUE_FAIL);
          $route.reload();
        });
    };

    /* 回填信息 */
    $scope.edit = function(){
      var modalInstance = $modal.open({
        templateUrl: 'views/orderWrite.html',
        controller: 'OrderWriteCtrl',
        resolve: {
          planeOrder: function(){
            return angular.copy($scope.record);
          }
        }
      });

      modalInstance.result.then(function (planeOrder) {
        $scope.record = planeOrder;
      });
    };

    /* 修改订单 */
    $scope.editOrder = function(){
      var modalInstance = $modal.open({
        templateUrl: 'views/planeOrderModal.html',
        controller: 'PlaneOrderModalCtrl',
        resolve: {
          item: function(){
            return angular.copy($scope.record);
          }
        }
      });

      modalInstance.result.then(function (planeOrder) {
        $scope.record = planeOrder;
      });
    };

  });
