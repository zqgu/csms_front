'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:NewstaffCtrl
 * @description
 * # NewstaffCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('PwdCtrl', function ($scope, $http, $routeParams) {

    $scope.record = {};

    /*重置交易密码*/
    $scope.save = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.post('api/users/' + $routeParams.id + '/rPwd', $scope.record);
      }
    };

    /*重置*/
    $scope.reset = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.resetForm(true);//清除错误信息
      $scope.record = {};
    };

    $scope.fields = {
      password: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'confirmPwd'
          }
        }
      },
      confirmPwd: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'password'
          }
        }
      }
    }
  });
