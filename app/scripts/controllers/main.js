'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('MainCtrl', function ($scope, treejson) {
    $scope.treejson = treejson;
  });
