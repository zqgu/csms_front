'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsRefundCtrl', function ($scope, $http, bzDicts, $routeParams, localStorageService, $route) {
    var getUrl = 'api/tickets/refunds/';
    var putUrl = 'api/tickets/refunds/order';

    var UNREFUND = $scope.UNREFUND = '1';//未退票
    var REFUNDING = $scope.REFUNDING = '2';//退票中
    var REFUNDED = $scope.REFUNDED = '3';//已退票
    var REFUND_FAIL = $scope.REFUND_FAIL = '4';//退票失败

    var curInterRefundState = localStorageService.get('CUR_REFUND_STATE');
    $scope.curState = curInterRefundState? curInterRefundState : UNREFUND;

    bzDicts.query('TICKET_REFUND_STATE')
      .then(function(data){
        $scope.ticketRefundStates = data;
      });

    /**
     * 查询国际退票详细
     */
    $http.get(getUrl + $routeParams.id + '/order')
      .success(function (data) {
        $scope.record = data.results;
        $scope.refunds = {
          UNREFUND: [],
          REFUNDING: [],
          REFUNDED: [],
          'REFUND_FAIL': []
        };
        _.each(data.results.refunds, function(refund){//将改签按状态分组
          if(refund.state === UNREFUND){
            $scope.refunds.UNREFUND.push(refund);
          }else if(refund.state === REFUNDING){
            $scope.refunds.REFUNDING.push(refund);
          }else if(refund.state === REFUNDED){
            $scope.refunds.REFUNDED.push(refund);
          }else if(refund.state === REFUND_FAIL){
            $scope.refunds.REFUND_FAIL.push(refund);
          }
        });

        if($scope.curState === UNREFUND){
          $scope.refunds.UNREFUND.state=true;
        }else if($scope.curState === REFUNDING){
          $scope.refunds.REFUNDING.state=true;
        }else if($scope.curState === REFUNDED){
          $scope.refunds.REFUNDED.state=true;
        }else if($scope.curState === REFUND_FAIL){
          $scope.refunds.REFUND_FAIL.state=true;
        }

        evalTicketNumbers();
      });


    /**
     * 开始退票
     */
    $scope.beginRefund = function(){
      if(!$scope.selected || $scope.selected.length === 0){
        return alert('请选择需要完成退票的机票');
      }
      var refunds = _.map($scope.selected, function(v){
        return {refundId: v.refundId};
      });
      $http.put(putUrl, {state: REFUNDING, refunds: refunds})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_REFUND_STATE', REFUNDING);
            $route.reload();
          }
        });
    };

    /**
     * 完成退票
     */
    $scope.endRefund = function(){
      if(!$scope.selected || $scope.selected.length === 0){
        return alert('请选择需要完成退票的机票');
      }
      var refunds = _.map($scope.selected, function(v){
        var tickets = _.map(v.refundTickets, function(t){
          return {
            //planeTicketNo: t.planeTicketNo,
            supRefundMoney: t.supRefundMoney,
            supHandlingFee: t.supHandlingFee,
            purRefundMoney: t.purRefundMoney,
            purHandlingFee: t.purHandlingFee,
            planeTicketId: t.origTicket.planeTicketId
          }
        });
        return {
          refundId: v.refundId,
          separatePnr: v.separatePnr,
          completeRemark: v.completeRemark,
          refundTickets: tickets
        };
      });
      $http.put(putUrl, {state: REFUNDED, refunds: refunds})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_REFUND_STATE', REFUNDED);
            $route.reload();
          }
        });
    };

    /**
     * 退票失败
     */
    $scope.failRefund = function(){
      if(!$scope.selected || $scope.selected.length === 0){
        return alert('请选择需要退票失败的机票');
      }
      var refunds = _.map($scope.selected, function(v){
        return {refundId: v.refundId};
      });
      $http.put(putUrl, {state: REFUND_FAIL, refunds: refunds})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_REFUND_STATE', REFUND_FAIL);
            $route.reload();
          }
        });
    };

    /**
     * 计算各状态票数
     */
    function evalTicketNumbers(){
      $scope.refunds.UNREFUND.refundTickets = 0;
      $scope.refunds.REFUNDING.refundTickets = 0;
      $scope.refunds.REFUNDED.refundTickets = 0;
      $scope.refunds.REFUND_FAIL.refundTickets = 0;

      _.each($scope.refunds.UNREFUND, function(refund){
        $scope.refunds.UNREFUND.refundTickets += refund.refundTickets.length;
      });
      _.each($scope.refunds.REFUNDING, function(refund){
        $scope.refunds.REFUNDING.refundTickets += refund.refundTickets.length;
      });
      _.each($scope.refunds.REFUNDED, function(refund){
        $scope.refunds.REFUNDED.refundTickets += refund.refundTickets.length;
      });
      _.each($scope.refunds.REFUND_FAIL, function(refund){
        $scope.refunds.REFUND_FAIL.refundTickets += refund.refundTickets.length;
      });
    }

  });
