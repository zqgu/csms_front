'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsIssuesCtrl', function ($scope, $http, bzIssue, bzDicts, localStorageService) {
    var url = 'api/planeOrders';

    var UNISSUE = $scope.UNISSUE = '2';//未出票
    var ISSUEING = $scope.ISSUEING = '3';//出票中
    var ISSUED = $scope.ISSUED = '4';//已出票
    var ISSUE_FAIL = $scope.ISSUE_FAIL = '5';//已出票

    bzDicts.query('TICKET_STATE')
      .then(function(data){
        $scope.ticketStates = data;
        _.map(data, function(v){
          v.code = v.code === '-1'? v.code : (parseInt(v.code) + 1).toString();
          return v;
        })
      });

    //查询条件
    $scope.queryParams = {
      no: 1,//初始页面
      limit: 10,//每页数据记录个数
      state: $scope.UNISSUE//出票订单状态
    };

    /**
     * 查询国际出票订单
     */
    $scope.query = function () {
      $http.get(url, {params: $scope.queryParams})
        .success(function (data) {
          $scope.records = data.results;
          $scope.totalItems = data.total;
      });
    };

    /* 查询订单数 */
    $http.get('api/planeOrders/counts/state')
      .success(function (data) {
        $scope.counts = data.results;
      });

    /* 切换出票状态 */
    $scope.switch = function(state){

      localStorageService.set('CUR_ISSUE_STATE', state);

      //查询条件
      $scope.queryParams = {
        no: 1,//初始页面
        limit: 10,//每页数据记录个数
        state: state? state : UNISSUE,//退票订单状态,
        startTime: null,
        endTime: null
      };
      $scope.query();
    };

    $scope.curState = localStorageService.get('CUR_ISSUE_STATE');
    $scope.issues = {
      UNISSUE: [],
      ISSUEING: [],
      ISSUED: [],
      ISSUE_FAIL: []
    };
    if($scope.curState === UNISSUE){
      $scope.issues.UNISSUE.state=true;
    }else if($scope.curState === ISSUEING){
      $scope.issues.ISSUEING.state=true;
    }else if($scope.curState === ISSUED){
      $scope.issues.ISSUED.state=true;
    }else if($scope.curState === ISSUE_FAIL){
      $scope.issues.ISSUE_FAIL.state=true;
    }

  });
