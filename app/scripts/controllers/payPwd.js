'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:NewstaffCtrl
 * @description
 * # NewstaffCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('PayPwdCtrl', function ($scope, $http, $routeParams) {

    $scope.record = {};

    /*重置交易密码*/
    $scope.save = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.post('api/users/' + $routeParams.id + '/rPayPwd', $scope.record);
      }
    };

    /*重置*/
    $scope.reset = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.resetForm(true);//清除错误信息
      $scope.record = {};
    };

    $scope.fields = {
      payPassword: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'confirmPwd'
          },
          regexp: {
            regexp: /^\d{6}$/,
            message: '支付密码必须为6位数字'
          }
        }
      },
      confirmPwd: {
        validators: {
          notEmpty: {},
          digits: {

          },
          identical: {
            field: 'payPassword'
          }
        }
      }
    }
  });
