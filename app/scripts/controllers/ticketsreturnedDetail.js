'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsreturnedCtrl
 * @description
 * # TicketsreturnedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsreturnedDetailCtrl', function ($scope, $http, $routeParams, $route, bzRefund, $modal, bzDicts) {

    var refundId = $routeParams.id;

    $scope.showRefundingBtn = false;//显示退票中按钮

    $scope.UNREFUND = '1';//未退票
    $scope.REFUNDING = '2';//退票中
    $scope.REFUNDED = '3';//已退票
    $scope.REFUND_FAIL = '4';//退票失败

    $scope.state = bzRefund.getState();//当前退票状态

    $scope.tickets1 = [];//未退票
    $scope.tickets2 = [];//退票中
    $scope.tickets3 = [];//已退票
    $scope.tickets4 = [];//退票失败

    $scope.collapse1 = false;
    $scope.collapse2 = false;

    bzDicts.query('PAY_TYPE')//支付方式
      .then(function(data){
        $scope.payTypes = data;
      });

    var query = function() {
      $http.get('api/tickets/refunds/'+ refundId + '/order')
        .success(function(data){
          $scope.record = data.results;
          $scope.tickets1 = [];//未退票
          $scope.tickets2 = [];//退票中
          $scope.tickets3 = [];//已退票
          $scope.tickets4 = [];//退票失败

          var tickets = data.results.passengers;
          tickets.forEach(function(v) {
            if(!v.refund) {
              return ;
            }

            if(v.refund.state === $scope.UNREFUND) {
              $scope.tickets1.push(v);
            }else if(v.refund.state === $scope.REFUNDING) {
              $scope.tickets2.push(v);
              if(v.refund.isCurrentUsed){
                $scope.showRefundingBtn = true;
              }
            }else if(v.refund.state === $scope.REFUNDED) {
              $scope.tickets3.push(v);
            }else if(v.refund.state === $scope.REFUND_FAIL) {
              $scope.tickets4.push(v);
            }

          });
        });
    };
    query();

    $scope.checkboxes = {items: {}};//待退票checkbox
    $scope.checkboxes2 = {items: {}};//退票中checkbox

    var checkedTickets;//选中的退票

    $scope.$watch('checkboxes.items', function(){//监听checkbox变化
      checkedTickets = [];
      _.each($scope.tickets1, function(v, i){
        var checked = $scope.checkboxes.items[i];
        if(checked){
          checkedTickets.push(v.refund.refundId);
        }
      });
    }, true);

    $scope.$watch('checkboxes2.items', function(){//监听checkbox变化
      checkedTickets = [];
      _.each($scope.tickets2, function(v, i){
        var checked = $scope.checkboxes2.items[i];
        if(checked){
          checkedTickets.push(v.refund);
        }
      });
    }, true);

    $scope.refund = function() {//开始退票
      if('undefined' == typeof checkedTickets || checkedTickets.length === 0) {
        return alert('请选择退票！');
      }

      $http.put('api/tickets/refunds/order', {refundIds: checkedTickets, state: 2})
        .success(function(data) {
          if(data.success){
            bzRefund.setState($scope.REFUNDING);
            $route.reload();
          }
        });
    };

    $scope.finish = function() {//完成退票
      if(checkedTickets.length === 0){
        return alert('请选择退票！');
      }
      var refunds = _.map(checkedTickets, function(v){
        v.handlingFee += v.bakSheesh;
        return v;
      });
      $http.put('api/tickets/refunds/order', {state: $scope.REFUNDED, refunds: refunds})
        .success(function(data) {
          if(data.success){
            bzRefund.setState($scope.REFUNDED);
            $route.reload();
          }
        });
    };

    /* 回填信息 */
    $scope.edit = function(passenger){
      var modalInstance = $modal.open({
        templateUrl: 'views/refundWrite.html',
        controller: 'RefundWriteCtrl',
        resolve: {
          passenger: function(){
            return angular.copy(passenger);
          }
        }
      });

      modalInstance.result.then(function (p) {
        $scope.tickets2.forEach(function(v, i){
          if(v.planeTicketId === p.planeTicketId){
            $scope.tickets2[i] = p;
          }
        });
      });
    };

    /* 退票失败 */
    $scope.fail = function(){
      if(checkedTickets.length === 0){
        return alert('请选择退票！');
      }
      $http.put('api/tickets/refunds/order', {state: $scope.REFUND_FAIL, refunds: checkedTickets})
        .success(function(data) {
          if(data.success){
            bzRefund.setState($scope.REFUND_FAIL);
            $route.reload();
          }
        });
    };

    $scope.toggle1 = function(event){//展开或折叠
      $scope.collapse1 = !$scope.collapse1;
      var hight = $(event.target).next().height();
      if($scope.collapse1){
        $('.slide1').css({height: hight});
      }else{
        $('.slide1').css({'height': '0'});
      }
    };

    $scope.toggle2 = function(event){//展开或折叠
      $scope.collapse2 = !$scope.collapse2;
      var hight = $(event.target).next().height();
      if($scope.collapse2){
        $('.slide2').css({height: hight});
      }else{
        $('.slide2').css({'height': '0'});
      }
    }

  });
