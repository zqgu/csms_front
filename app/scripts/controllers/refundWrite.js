'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('RefundWriteCtrl', function ($scope, $http, $modalInstance, passenger, bzDicts) {

    $scope.passenger = passenger;

    bzDicts.query('PAY_TYPE')//支付方式
      .then(function(data){
        $scope.payTypes = data;
        $scope.passenger.refund.way = $scope.payTypes[0].code;
      });

    $scope.save = function(){//保存
      $modalInstance.close(passenger);
    };

    $scope.cancel = function(){//取消
      $modalInstance.dismiss('cancel');
    }

  });
