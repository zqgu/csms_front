angular.module('projApp')
  .controller('TicketsChangesDetailCtrl', function ($scope, $http, $routeParams, $route, $modal, localStorageService, bzDicts) {'use strict';

    var changeId = $routeParams.id;
    var url = 'api/tickets/changes/order';
    var me = localStorageService.get('user');

    $scope.TICKT_CHANGE_STATE_UNCHANGE = '1';//未改签
    $scope.TICKT_CHANGE_STATE_CHANGEING = '2';//改签中
    $scope.TICKT_CHANGE_STATE_CHANGED = '3';//已改签
    $scope.TICKT_CHANGE_STATE_FAIL = '4';//改签失败

    $scope.state = localStorageService.get('changeState');//当前改签状态

    /*$scope.tickets1 = [];//未改签
    $scope.tickets2 = [];//改签中
    $scope.tickets3 = [];//已改签
    $scope.tickets4 = [];//改签失败*/

    /**
     * 缓存查询支付方式
     */
    bzDicts.query('PAY_TYPE')
      .then(function(data){
        $scope.payTypes = data;
      });

    /**
     * 查询改签
     */
    function query() {
      $http.get('api/tickets/changes/'+ changeId + '/order')
        .success(function(data){
          $scope.showChangeingBtn = false;
          $scope.record = data.results;
          $scope.tickets1 = [];//未改签
          $scope.tickets2 = [];//改签中
          $scope.tickets3 = [];//已改签
          $scope.tickets4 = [];//改签失败

          var tickets = data.results.passengers;
          //将机票按改签装填分组
          tickets.forEach(function(v) {
            var change;
            if(v.change){
              change = v.change;
              if(change.state === $scope.TICKT_CHANGE_STATE_UNCHANGE){
                $scope.tickets1.push(v);
              }else if(change.state === $scope.TICKT_CHANGE_STATE_CHANGEING){
                if(change.handlingEmployee && change.handlingEmployee.id === me.employeeId){
                  $scope.showChangeingBtn = true
                }
                $scope.tickets2.push(v);
              }else if(change.state === $scope.TICKT_CHANGE_STATE_FAIL){
                $scope.tickets4.push(v);
              }
            }else if(v.ticketChanged && v.ticketChanged[0]){
              change = v.ticketChanged[0].change;
              if(change.state === $scope.TICKT_CHANGE_STATE_CHANGED){
                $scope.tickets3.push(v.ticketChanged[0]);
              }
            }
          });
        });
    }
    query();

    $scope.checkboxes = {items: {}};//待改签checkbox
    $scope.checkboxes2 = {items: {}};//改签中checkbox

    var checkedTickets = [];//选中的机票

    /**
     * 监听未改签的checkbox
     */
    $scope.$watch('checkboxes.items', function(){
      checkedTickets = [];
      _.each($scope.tickets1, function(v){
        var checked = $scope.checkboxes.items[v.planeTicketId];
        if(checked){
          checkedTickets.push(v.change.changeId);
        }
      });
    }, true);

    /**
     * 监听改签中的checkbox
     */
    $scope.$watch('checkboxes2.items', function(){
      checkedTickets = [];
      _.each($scope.tickets2, function(v){
        var checked = $scope.checkboxes2.items[v.planeTicketId];
        if(checked){
          checkedTickets.push(v);
        }
      });
    }, true);

    /**
     * 开始改签
     * @returns {*}
     */
    $scope.change = function() {
      if(checkedTickets.length === 0) {
        return alert('请选择改签！');
      }

      $http.put(url, {changeIds: checkedTickets, state: $scope.TICKT_CHANGE_STATE_CHANGEING})
        .success(function(data) {
          if(data.success){
            localStorageService.set('changeState', $scope.TICKT_CHANGE_STATE_CHANGEING);
            $route.reload();
          }
        });
    };

    /**
     * 完成改签
     * @returns {*}
     */
    $scope.finish = function() {
      if(checkedTickets.length === 0){
        return alert('请选择改签！');
      }
      var changes = _.map(checkedTickets, function(v){
        return {
          changeId : v.change.changeId,
          pnr: v.change.pnr,
          priceDifference: v.change.priceDifference,//采购商改签差价
          handlingFee: v.change.handlingFee,//采购商手续费
          //priceDifference: v.change.salePrice - v.salePrice,
          supPriceDifference: v.change.supPriceDifference,//供应商改签差价
          supHandlingFee: v.change.supHandlingFee,//供应商手续费
          remark : v.change.remark,
          planeTicket : {
            salePrice : v.change.salePrice,
            planeTicketNo : v.change.planeTicketNo
          },
          origPlaneTicket : {
            planeTicketId : v.planeTicketId
            //planeTicketNo : "001"
          },
          flight : {
            planeTripId : v.change.planeTripId
          },
          origFlight : {
            planeTripId : v.change.origPlaneTripId
          }
        }
      });
      $http.put(url, {changes: changes, state: $scope.TICKT_CHANGE_STATE_CHANGED})
        .success(function(data) {
          if(data.success){
            localStorageService.set('changeState', $scope.TICKT_CHANGE_STATE_CHANGED);
            $route.reload();
          }
        });
    };

    /**
     * 改签失败
     * @returns {*}
     */
    $scope.fail = function(){
      if(checkedTickets.length === 0){
        return alert('请选择改签！');
      }
      var changes = _.map(checkedTickets, function(v) {
        return {changeId : v.change.changeId};
      });
      $http.put(url, {state: $scope.TICKT_CHANGE_STATE_FAIL, changes: changes})
        .success(function(data) {
          if(data.success){
            localStorageService.set('changeState', $scope.TICKT_CHANGE_STATE_FAIL);
            $route.reload();
          }
        });
    };

    /**
     * 回填信息
     * @param passenger 机票
     */
    $scope.edit = function(passenger){
      var modalInstance = $modal.open({
        templateUrl: 'views/changeWrite.html',
        controller: 'ChangeWriteCtrl',
        resolve: {
          passenger: function(){
            return angular.copy(passenger);
          }
        }
      });

      modalInstance.result.then(function (p) {
        $scope.tickets2.forEach(function(v, i){
          if(v.planeTicketId === p.planeTicketId){
            $scope.tickets2[i] = p;
          }
        });
      });
    };

  });
