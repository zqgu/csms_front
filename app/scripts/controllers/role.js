'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:RolesCtrl
 * @description
 * # RolesCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('RoleCtrl', function ($scope, $http, Restangular) {
    //查询条件
    $scope.query = {
      no: 1,//初始页面
      limit: 10//每页数据记录个数
    };

    var fetch = $scope.fetch = function () {
      console.log($scope.query);
      //请求部门列表数据
      $http.get('api/roles', {params: $scope.query}).success(function (data) {
        $scope.records = data.results;
        $scope.totalItems = data.total;
      });
    };

    /*删除*/
    $scope.del = function(index){
      return {
        onConfirm: function(event){
          event.preventDefault();
          Restangular.one('roles', $scope.records[index].roleId).remove()
            .then(function(){
              fetch();
            });
        }
      };
    };

    fetch();
  });
