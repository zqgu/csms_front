'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:WelcomeCtrl
 * @description
 * # WelcomeCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('WelcomeCtrl', function ($scope, localStorageService, bzMessagesService,$location) {
    $scope.record = {
      name: 'OneWay',
      query: {}
    };

    $scope.StopOver = {name:'StopOver',query:[{},{}]};
    $scope.$on('bzMessagesService:getUnreads',function(data,result){
      console.log(data);
      $scope.unReads = result;
    });
    //多程添加航程
    $scope.addFlight = function(){
      if($scope.StopOver.query.length < 6){
        $scope.StopOver.query.push({});
      }
    };

    $scope.query = function () {
      var data = angular.copy($scope.record);

      //针对联程的处理
      if(data.name === 'StopOver') {
        data = angular.copy($scope.StopOver);
      }

      console.log(data);
      //将查询参数保存到缓存中，供其它页面使用，如查询页面
      localStorageService.set('flightsQuery', data);
      //跳转视图
      $location.path('/flights');
    };

    //获取当前用户
    if(localStorageService.get('user')){
      $scope.user = localStorageService.get('user');
      console.log($scope.user);
    }

  });
