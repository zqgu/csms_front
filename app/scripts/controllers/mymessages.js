'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:MymessagesCtrl
 * @description
 * # MymessagesCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('MymessagesCtrl', function ($scope, $http) {
    var url = 'api/msgAttentions';
    //查询条件
    $scope.query = {
      no: 1,//初始页面
      limit: 10//每页数据记录个数
    };

    var fetch = $scope.fetch = function () {
      console.log($scope.query);
      //请求部门列表数据
      $http.get(url, {params: $scope.query}).success(function (data) {
        $scope.records = data.results;
        $scope.totalItems = data.total;
      });
    };

    fetch();
  });
