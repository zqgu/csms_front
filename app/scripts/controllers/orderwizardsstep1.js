'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:Orderwizardsstep1Ctrl
 * @description
 * # Orderwizardsstep1Ctrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('Orderwizardsstep1Ctrl', function ($scope, $location, localStorageService, $http) {

    $scope.step = 1;

    //根据手机号确认用户是否存在
    $scope.checkUserByMobile = function (){
      var url = 'api/employees/' + $scope.mobile + '/selectUserByMobile';
      
      $http.get(url).success(function (data){
        if(!data.results){
          $scope.noUser = true;
        }else{
          $scope.noUser = false;
          $scope.apply = data.results;
          $scope.step = 2;
        }
      });
    };

    $scope.gotoStep1 = function () {
      $scope.step = 1;
      $scope.noUser = false;
      $scope.apply = null;
      localStorageService.remove('applyUserInfo');
      localStorageService.remove('flightsQuery');
    };

    $scope.confirmUser = function () {
      $scope.step = 3;
      localStorageService.set('applyUserInfo', $scope.apply);
    };

    $scope.query = {
      type: 'OW'
    };

    $scope.execute = function () {
      
      var data = angular.copy($scope.query);
      var results;
      var qType = data.type;

      //往返程
      if (qType === 'RT') {
        results = [{
          dep: data.dep,
          arr: data.arr,
          depDate: data.depDate
        },{
          dep: data.arr,
          arr: data.dep,
          depDate: data.backDate
        }];
      }
      //联程
      else if (qType === 'MC') {
        results = [{
          dep: data.dep,
          arr: data.trans,
          depDate: data.depDate
        },{
          dep: data.trans,
          arr: data.arr,
          depDate: data.transDate
        }];
      }
      //单程
      else if (qType === 'OW') {
        results = [{
          dep: data.dep,
          arr: data.arr,
          depDate: data.depDate
        }];
      }

      console.log(results);

      localStorageService.set('flightsQuery', {
        type: qType,
        queries: results
      });

      $location.path('/flights');
    };

    var init = function () {
      var applyUserInfo = localStorageService.get('applyUserInfo');

      if(applyUserInfo){
        $scope.noUser = false;
        $scope.apply = applyUserInfo;
        $scope.step = 2;
      }
    };

    init();

  });
