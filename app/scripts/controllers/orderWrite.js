'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('OrderWriteCtrl', function ($scope, $http, $modalInstance, planeOrder, bzDicts) {

    $scope.planeOrder = planeOrder;

    bzDicts.query('PAY_TYPE')//支付方式
      .then(function(data){
        $scope.payTypes = data;
        $scope.supPayType = $scope.payTypes[0].code;
      });

    $http.get('api/suppliers')
      .success(function(data){
        $scope.sups = data.results;
        $scope.supId = $scope.sups[0].supplierId;
      });

    $scope.save = function(){//保存
      $scope.planeOrder.state = 4;
      $scope.planeOrder.supId = $scope.supId;

      $scope.planeOrder.detailSupPlaneOrder = {
        supOrderNo: $scope.supOrderNo,
        supPayPrice: $scope.supPayPrice,
        supPayType: $scope.supPayType
      };

      $modalInstance.close($scope.planeOrder);
    };

    $scope.cancel = function(){//取消
      $modalInstance.dismiss('cancel');
    }

  });
