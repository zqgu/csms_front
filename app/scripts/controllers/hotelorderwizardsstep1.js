'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:Orderwizardsstep1Ctrl
 * @description
 * # Orderwizardsstep1Ctrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('Hotelorderwizardsstep1Ctrl', function ($scope, $location, localStorageService, $http, bzPublishFuns) {

    $scope.step = 1;

    //根据手机号确认用户是否存在
    $scope.checkUserByMobile = function (){
      var url = 'api/employees/' + $scope.mobile + '/selectUserByMobile';

      $http.get(url).success(function (data){
        if(!data.results){
          $scope.noUser = true;
        }else{
          $scope.noUser = false;
          $scope.apply = data.results;
          $scope.step = 2;
        }
      });
    };

    $scope.gotoStep1 = function () {
      $scope.step = 1;
      $scope.noUser = false;
      $scope.apply = null;
      localStorageService.remove('applyUserInfo');
      localStorageService.remove('flightsQuery');
    };

    $scope.confirmUser = function () {
      $scope.step = 3;
      localStorageService.set('applyUserInfo', $scope.apply);
    };

    $scope.query = {
      type: 'OW'
    };

    $scope.execute = function () {

      var data = angular.copy($scope.query);
      var results;
      var qType = data.type;

      //往返程
      if (qType === 'RT') {
        results = [{
          dep: data.dep,
          arr: data.arr,
          depDate: data.depDate
        },{
          dep: data.arr,
          arr: data.dep,
          depDate: data.backDate
        }];
      }
      //联程
      else if (qType === 'MC') {
        results = [{
          dep: data.dep,
          arr: data.trans,
          depDate: data.depDate
        },{
          dep: data.trans,
          arr: data.arr,
          depDate: data.transDate
        }];
      }
      //单程
      else if (qType === 'OW') {
        results = [{
          dep: data.dep,
          arr: data.arr,
          depDate: data.depDate
        }];
      }

      console.log(results);

      localStorageService.set('flightsQuery', {
        type: qType,
        queries: results
      });

      $location.path('/flights');
    };

    var init = function () {
      var applyUserInfo = localStorageService.get('applyUserInfo');

      if(applyUserInfo){
        $scope.noUser = false;
        $scope.apply = applyUserInfo;
        $scope.step = 2;
      }
    };

    init();

    $scope.order = function(){
      var url = 'api/elong/hotel/test/' + 10101129;
      $scope.waiting = $http.get(url);

      $scope.waiting.success(function (data) {
        var hotel = data.results[0];
        var room = hotel.rooms[0];
        var ratePlan = hotel.rooms[0].ratePlans[2];
        $scope.toBook(ratePlan,room,hotel);
      });
    };

    //预订
    $scope.toBook = function(ratePlan,room,record){

      var sRatePlan = ratePlan;
      var sRoom = room;
      var sRecord = record;

      /*--------staticData----------*/
      var staticData = _.pick(sRecord.staticData, 'review', 'detail');

      //取出静态房间信息
      var tsRoom = _.find(sRecord.staticData.rooms,function(r){
        if( r.id === sRoom.roomId){
          return true;
        }
      });

      //取出对应房间的图片
      var tsImage = _.find(sRecord.staticData.images,function(img){
        if(img.roomId + '' === sRoom.roomId){
          return true;
        }
      });

      if(!tsImage){
        tsImage = _.find(sRecord.staticData.images,function(img){
          if(img.isCoverImage){
            return true;
          }
        });

      }
      staticData.image = tsImage;
      staticData.room = tsRoom;


      /*--------dynamicData----------*/

      var dynamic = _.pick(sRecord, 'hotelId', 'lowRate' ,'currencyCode','distance');

      //取出动态房间信息
      var tdRoom = _.find(record.rooms,function(room){
        return room.roomId === sRoom.roomId;
      });

      dynamic.room = sRoom;
      dynamic.ratePlan = sRatePlan;

      //预订规则
      dynamic.bookingRules = bzPublishFuns.getByKeys('bookingRuleId',sRatePlan.bookingRuleIds,record.bookingRules);

      //担保规则
      dynamic.guaranteeRules = bzPublishFuns.getByKeys('guranteeRuleId',sRatePlan.guaranteeRuleIds,record.guaranteeRules);

      //预付规则
      dynamic.prepayRules = bzPublishFuns.getByKeys('prepayRuleId',sRatePlan.prepayRuleIds,record.prepayRules);

      //附加服务
      dynamic.valueAdds = bzPublishFuns.getByKeys('valueAddId',sRatePlan.valueAddIds,record.valueAdds);

      //促销规则
      dynamic.drrRules = bzPublishFuns.getByKeys('drrRuleId',sRatePlan.drrRuleIds,record.drrRules);

      //礼品
      dynamic.gifts = bzPublishFuns.getByKeys('giftId',sRatePlan.giftIds,record.gifts);

      //政策
      dynamic.hAvailPolicys = bzPublishFuns.getByKeys('id',sRatePlan.hAvailPolicyIds,record.hAvailPolicys);

      var data = { staticData:staticData,dynamic:dynamic };

      localStorageService.set('bookData', data);

      $location.path('/hotels/orders/create');
    };


  });
