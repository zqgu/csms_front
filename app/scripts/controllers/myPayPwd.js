'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:NewstaffCtrl
 * @description
 * # NewstaffCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('MyPayPwdCtrl', function ($scope, $http) {

    $scope.forget = false;
    $scope.record1 = {};//确认交易密码
    $scope.record2 = {};//重置交易密码

    /*确认交易密码*/
    $scope.confirmMyPayPwd = function(){
      var validator = $('form[name=confirmForm]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()) {
        $http.post('api/users/confirmPayPwd', $scope.record1)
          .error(function () {
            validator.updateStatus('confirmPassword', 'INVALID', 'serverPayPwd');
          });
      }
    };

    /*确认交易密码重置*/
    $scope.resetConfrim = function(){
      var validator = $('form[name=confirmForm]').data('bootstrapValidator');
      validator.resetForm(true);//清除错误信息
    };

    /*发送邮件获得临时交易密码*/
    $scope.send = function(){
      $http.post('api/users/getTempPayPassword', {})
        .success(function(){
          $scope.forget = !$scope.forget;
        });
    };

    /*重置交易密码*/
    $scope.save = function(){
      $scope.resetAlert = true;
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.post('api/my/rPayPwd', $scope.record2)
          .error(function(){
            validator.updateStatus('payPassword', 'INVALID', 'serverPayPwd');
          });
      }
    };

    /*重置交易密码重置*/
    $scope.reset = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.resetForm(true);//清除错误信息
      $scope.record2 = {};
    };

    $scope.returnPayPwd = function(){
      $scope.forget = !$scope.forget;
    };

    /*确认交易密码*/
    $scope.confirmFields = {
      confirmPassword: {
        validators: {
          notEmpty: {},
          payPassword: {},
          serverPayPwd: {}
        }
      }
    };

    /*重置交易密码*/
    $scope.fields = {
      payPassword: {
        validators: {
          notEmpty: {},
          payPassword: {},
          serverPayPwd: {}
        }
      },
      newPayPassword: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'confirmPwd'
          },
          payPassword: {}
        }
      },
      confirmPwd: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'newPayPassword'
          }
        }
      }
    }
  });
