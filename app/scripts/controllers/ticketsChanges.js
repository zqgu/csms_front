'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsChangesCtrl', function ($scope, $http, bzDicts, localStorageService) {
    var url = 'api/tickets/changes';

    var UNCHANGE = $scope.UNCHANGE = '1';//未改签
    var CHANGING = $scope.CHANGING = '2';//改签中
    var CHANGED = $scope.CHANGED = '3';//已改签
    var CHANGE_FAIL = $scope.CHANGE_FAIL = '4';//已改签

    bzDicts.query('TICKET_CHANGE_STATE')
      .then(function(data){
        $scope.ticketChangeStates = data;
      });

    /**
     * 查询国际改签
     */
    $scope.query = function () {
      $http.get(url, {params: $scope.queryParams})
        .success(function (data) {
          $scope.records = data.results;
          $scope.totalItems = data.total;
      });
    };

    $http.get('api/tickets/changes/counts/state')
      .success(function (data) {
        $scope.counts = data.results;
      });

    /* 切换改签状态 */
    $scope.switch = function(state){

      localStorageService.set('CUR_CHANGE_STATE', state);

      //查询条件
      $scope.queryParams = {
        no: 1,//初始页面
        limit: 10,//每页数据记录个数
        state: state? state : UNCHANGE,//改签订单状态
        startTime: null,
        endTime: null
      };
      $scope.query();
    };

    $scope.curState = localStorageService.get('CUR_CHANGE_STATE');
    $scope.changes = {
      UNCHANGE: [],
      CHANGING: [],
      CHANGED: [],
      CHANGE_FAIL: []
    };
    if($scope.curState === UNCHANGE){
      $scope.changes.UNCHANGE.state=true;
    }else if($scope.curState === CHANGING){
      $scope.changes.CHANGING.state=true;
    }else if($scope.curState === CHANGED){
      $scope.changes.CHANGED.state=true;
    }else if($scope.curState === CHANGE_FAIL){
      $scope.changes.CHANGE_FAIL.state=true;
    }

  });
