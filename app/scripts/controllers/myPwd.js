'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:NewstaffCtrl
 * @description
 * # NewstaffCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('MyPwdCtrl', function ($scope, $http) {

    $scope.record = {};//重置交易密码

    /*重置交易密码*/
    $scope.save = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.post('api/my/rPwd', $scope.record)
          .error(function(){
            validator.updateStatus('password', 'INVALID', 'serverPwd');
          });
      }
    };

    /*重置*/
    $scope.reset = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.resetForm(true);//清除错误信息
      $scope.record = {};
    };

    $scope.fields = {
      password: {
        validators: {
          notEmpty: {},
          serverPwd: {}
        }
      },
      newPassword: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'confirmPwd'
          }
        }
      },
      confirmPwd: {
        validators: {
          notEmpty: {},
          identical: {
            field: 'newPassword'
          }
        }
      }
    }
  });
