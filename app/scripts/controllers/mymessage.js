'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:MymessageCtrl
 * @description
 * # MymessageCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('MymessageCtrl', function ($scope, $routeParams, bzMessagesService) {
    var id = $routeParams.id;

    //查询记录
    bzMessagesService.get(id).success(function (data) {
      $scope.record = data.results;

      console.log($scope.record);
      
      var state = $scope.record.state;

      // 如果状态为未读
      if(state && state.code === '0') {
        bzMessagesService.read(id);
      }

    });

  });
