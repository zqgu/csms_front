angular.module('projApp')
  .controller('ChangeWriteCtrl', function ($scope, $http, $modalInstance, passenger, bzDicts) {'use strict';

    $scope.passenger = passenger;

    bzDicts.query('PAY_TYPE')//支付方式
      .then(function(data){
        $scope.payTypes = data;
        $scope.passenger.change.way = $scope.payTypes[0].code;
      });

    $scope.save = function(){//保存
      $modalInstance.close(passenger);
    };

    $scope.cancel = function(){//取消
      $modalInstance.dismiss('cancel');
    }

  });
