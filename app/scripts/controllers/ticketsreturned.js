'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsreturnedCtrl
 * @description
 * # TicketsreturnedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsreturnedCtrl', function ($scope, $http, bzRefund) {

    $scope.UNREFUND = '1';//未退票
    $scope.REFUNDING = '2';//退票中
    $scope.REFUNDED = '3';//已退票
    $scope.REFUND_FAIL = '4';//退票失败

    bzRefund.setState($scope.UNREFUND);//当前退票状态

    var url = 'api/tickets/refunds';

    //查询条件
    $scope.queryParams = {
      no: 1,//初始页面
      limit: 10,//每页数据记录个数
      state: bzRefund.getState()
    };

    /* 查询退票 */
    var query = $scope.query = function () {
      $http.get(url, {params: $scope.queryParams})
        .success(function (data) {
          $scope.records = data.results;
          $scope.totalItems = data.total;
        });
    };

    /* 查询退票数量 */
    function queryCounts(){
      $http.get(url + '/counts', {params: {state: $scope.UNREFUND}})//查询未退票数量
        .success(function (data) {
          $scope.undefundCounts = data.results;
        });
      $http.get(url + '/counts', {params: {state: $scope.REFUNDING}})//查询退票中数量
        .success(function (data) {
          $scope.defundingCounts = data.results;
        });
    }

    /* 切换退票状态列表 */
    $scope.switch = function(state) {
      $scope.queryParams.state = state;
      $scope.queryParams.startTime = null;
      $scope.queryParams.endTime = null;
      bzRefund.setState(state);

      query();
    };

    queryCounts();

  });
