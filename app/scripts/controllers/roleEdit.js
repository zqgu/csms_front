'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:RoleCtrl
 * @description
 * # RoleCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('RoleEditCtrl', function ($scope, $http, $routeParams) {
    //资源地址
    var url = 'api/roles/' + $routeParams.id;

    $scope.record = {};

    //保存修改前的数据
    var originData;

    //查询记录
    $http.get(url).success(function (data) {
      $scope.record = data.results;
      originData = angular.copy(data.results);
    });

    /*提交*/
    $scope.submit = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.put(url, $scope.record).success(function (data) {
          angular.extend($scope.record, data.results);
        });
      }
    };

    /*重置*/
    $scope.reset = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.resetForm(false);//清除错误信息
      $scope.record = angular.copy(originData);
    };

    $scope.fields = {
      roleName: {
        validators: {
          notEmpty: {},
          stringLength: {
            max: 20
          }
        }
      },
      detail: {
        validators: {
          stringLength: {
            max: 200
          }
        }
      }
    }

  });
