'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:NewroleCtrl
 * @description
 * # NewroleCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('RoleNewCtrl', function ($scope, $http) {
    //初始化
    $scope.record = {};

    /*提交*/
    $scope.submit = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.post('api/roles', $scope.record).success(function (data) {
          $scope.record = {};
        });
      }
    };

    /*重置*/
    $scope.reset = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.resetForm(false);//清除错误信息
      $scope.record = {};
    };

    $scope.fields = {
      roleName: {
        validators: {
          notEmpty: {},
          stringLength: {
            max: 20
          }
        }
      },
      detail: {
        validators: {
          stringLength: {
            min: 0,
            max: 200
          }
        }
      }
    };

  });
