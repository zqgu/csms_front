'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsIssueCtrl', function ($scope, $http, bzDicts, $routeParams, localStorageService, $route) {
    var getUrl = 'api/planeOrders/';
    var putUrl = 'api/planeOrders/order';

    var UNISSUE = $scope.UNISSUE = '2';//未出票
    var ISSUEING = $scope.ISSUEING = '3';//出票中
    var ISSUED = $scope.ISSUED = '4';//已出票
    var ISSUE_FAIL = $scope.ISSUE_FAIL = '5';//已出票

    var curInterChangeState = localStorageService.get('CUR_ISSUE_STATE');
    $scope.curState = curInterChangeState? curInterChangeState : UNISSUE;

    $http.get('api/suppliers')
      .success(function(data){
        $scope.sups = data.results;
      });

    /**
     * 查询国际出票详细
     */
    $http.get(getUrl + $routeParams.id)
      .success(function (data) {
        data.results.passengers = _.map(data.results.passengers, function(p){
          p.state = (parseInt(p.state) + 1).toString();
          return p;
        });
        $scope.record = data.results;
        $scope.issues = {
          UNISSUE: [],
          ISSUEING: [],
          ISSUED: [],
          'ISSUE_FAIL': []
        };
        _.each(data.results.passengers, function(issue){//将出票按状态分组
          if(issue.state === UNISSUE){
            $scope.issues.UNISSUE.push(issue);
          }else if(issue.state === ISSUEING){
            $scope.issues.ISSUEING.push(issue);
          }else if(issue.state === ISSUED){
            $scope.issues.ISSUED.push(issue);
          }else if(issue.state === ISSUE_FAIL){
            $scope.issues.ISSUE_FAIL.push(issue);
          }
        });

        if($scope.curState === UNISSUE){
          $scope.issues.UNISSUE.state=true;
        }else if($scope.curState === ISSUEING){
          $scope.issues.ISSUEING.state=true;
        }else if($scope.curState === ISSUED){
          $scope.issues.ISSUED.state=true;
        }else if($scope.curState === ISSUE_FAIL){
          $scope.issues.ISSUE_FAIL.state=true;
        }

      });

    /**
     * 开始出票
     */
    $scope.beginIssue = function(){
      $http.get('api/planeOrders/' + $routeParams.id + '/outTicket')
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_ISSUE_STATE', ISSUEING);
            $route.reload();
          }
        });
    };

    /**
     * 完成出票
     */
    $scope.endIssue = function(){

      var order = {
        supId: $scope.issues.ISSUEING.supId,
        supPlaneOrderNo: $scope.issues.ISSUEING.supPlaneOrderNo,
        supPayType: $scope.issues.ISSUEING.supPayType,
        supBillNo: $scope.issues.ISSUEING.supBillNo,
        ticketingRemark: $scope.issues.ISSUEING.ticketingRemark,
        passengers: _.map($scope.issues.ISSUEING, function(v){
          return {
            planeTicketId: v.planeTicketId,
            planeTicketNo: v.planeTicketNo,
            floorPrice: v.floorPrice
          }
        })
      };
      $http.put('api/planeOrders/' + $routeParams.id + '/backfill', order)
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_ISSUE_STATE', ISSUED);
            $route.reload();
          }
        });
    };

    /**
     * 出票失败
     */
    $scope.failIssue = function(){
      $http.put('api/planeOrders/' + $routeParams.id + '/backfail', {})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_ISSUE_STATE', ISSUE_FAIL);
            $route.reload();
          }
        });
    };

  });
