'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('InterTicketsChangeCtrl', function ($scope, $http, bzDicts, $routeParams, localStorageService, $route) {
    var getUrl = 'api/inter/tickets/changes/';
    var putUrl = 'api/inter/tickets/changes/order';

    var UNCHANGE = $scope.UNCHANGE = '1';//未改签
    var CHANGING = $scope.CHANGING = '2';//改签中
    var CHANGED = $scope.CHANGED = '3';//已改签
    var CHANGE_FAIL = $scope.CHANGE_FAIL = '4';//改签失败

    var curInterChangeState = localStorageService.get('CUR_INTER_CHANGE_STATE');
    $scope.curState = curInterChangeState? curInterChangeState : UNCHANGE;

    bzDicts.query('TICKET_CHANGE_STATE')
      .then(function(data){
        $scope.ticketChangeStates = data;
      });

    /**
     * 查询国际改签详细
     */
    $http.get(getUrl + $routeParams.id + '/order')
      .success(function (data) {
        $scope.record = data.results;
        $scope.changes = {
          UNCHANGE: [],
          CHANGING: [],
          CHANGED: [],
          'CHANGE_FAIL': []
        };
        _.each(data.results.changes, function(change){//将改签按状态分组
          if(change.state === UNCHANGE){
            $scope.changes.UNCHANGE.push(change);
          }else if(change.state === CHANGING){
            $scope.changes.CHANGING.push(change);
          }else if(change.state === CHANGED){
            $scope.changes.CHANGED.push(change);
          }else if(change.state === CHANGE_FAIL){
            $scope.changes.CHANGE_FAIL.push(change);
          }
        });

        if($scope.curState === UNCHANGE){
          $scope.changes.UNCHANGE.state=true;
        }else if($scope.curState === CHANGING){
          $scope.changes.CHANGING.state=true;
        }else if($scope.curState === CHANGED){
          $scope.changes.CHANGED.state=true;
        }else if($scope.curState === CHANGE_FAIL){
          $scope.changes.CHANGE_FAIL.state=true;
        }

        evalTicketNumbers();

      });

    /**
     * 开始改签
     */
    $scope.beginChange = function(){
      if(!$scope.selected || $scope.selected.length === 0){
        return alert('请选择需要开始改签的机票');
      }
      var changes = _.map($scope.selected, function(v){
        return {changeId: v.changeId};
      });
      $http.put(putUrl, {state: CHANGING, changes: changes})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_INTER_CHANGE_STATE', CHANGING);
            $route.reload();
          }
        });
    };

    /**
     * 完成改签
     */
    $scope.endChange = function(){
      if(!$scope.selected || $scope.selected.length === 0){
        return alert('请选择需要完成改签的机票');
      }
      var changes = _.map($scope.selected, function(v){
        var tickets = _.map(v.changeTickets, function(t){
          return {
            planeTicketNo: t.planeTicketNo,
            supPriceDifference: t.supPriceDifference,
            supHandlingFee: t.supHandlingFee,
            purPriceDifference: t.purPriceDifference,
            purHandlingFee: t.purHandlingFee,
            origPlaneTicketId: t.origTicket.planeTicketId
          }
        });
        return {
          changeId: v.changeId,
          separatePnr: v.separatePnr,
          completeRemark: v.completeRemark,
          changeTickets: tickets
        };
      });
      $http.put(putUrl, {state: CHANGED, changes: changes})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_INTER_CHANGE_STATE', CHANGED);
            $route.reload();
          }
        });
    };

    /**
     * 改签失败
     */
    $scope.failChange = function(){
      if(!$scope.selected || $scope.selected.length === 0){
        return alert('请选择需要改签失败的机票');
      }
      var changes = _.map($scope.selected, function(v){
        return {changeId: v.changeId};
      });
      $http.put(putUrl, {state: CHANGE_FAIL, changes: changes})
        .success(function(data){
          if(data.success){
            localStorageService.set('CUR_INTER_CHANGE_STATE', CHANGE_FAIL);
            $route.reload();
          }
        });
    };

    /**
     * 计算各状态票数
     */
    function evalTicketNumbers(){
      $scope.changes.UNCHANGE.changeTickets = 0;
      $scope.changes.CHANGING.changeTickets = 0;
      $scope.changes.CHANGED.changeTickets = 0;
      $scope.changes.CHANGE_FAIL.changeTickets = 0;

      _.each($scope.changes.UNCHANGE, function(change){
        $scope.changes.UNCHANGE.changeTickets += change.changeTickets.length;
      });
      _.each($scope.changes.CHANGING, function(change){
        $scope.changes.CHANGING.changeTickets += change.changeTickets.length;
      });
      _.each($scope.changes.CHANGED, function(change){
        $scope.changes.CHANGED.changeTickets += change.changeTickets.length;
      });
      _.each($scope.changes.CHANGE_FAIL, function(change){
        $scope.changes.CHANGE_FAIL.changeTickets += change.changeTickets.length;
      });
    }

  });
