'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('PlaneOrderModalCtrl', function ($scope, $http, $modalInstance, item) {

    $scope.record = item;

    $scope.onePrice = 0;  //单人机票总额
    $scope.oneFuel = 0; //单人燃油总额
    $scope.oneAirport = 0; //单人机建总额

    _.each($scope.record.flights,function(f){
      $scope.onePrice = $scope.onePrice + parseInt(f.price);
      $scope.oneFuel = $scope.oneFuel + parseInt(f.fuelFee);
      $scope.oneAirport = $scope.oneAirport + parseInt(f.airportCstFee);
    });

    $scope.save = function(){//保存
      $http.post('api/settlements', $scope.record)
        .success(function(data){
          if(data.success){
            $modalInstance.close(true);
          }else{
            $modalInstance.close(false);
          }
        });
    };

    $scope.cancel = function(){//取消
      $modalInstance.dismiss('cancel');
    }

  });
