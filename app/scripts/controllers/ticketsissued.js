'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsissuedCtrl', function ($scope, $http, bzIssue) {
    var url = 'api/planeOrders';

    $scope.UNISSUE = '2';//未出票
    $scope.ISSUEING = '3';//出票中
    $scope.ISSUED = '4';//已出票
    $scope.ISSUE_FAIL = '5';//已出票

    //查询条件
    $scope.queryParams = {
      no: 1,//初始页面
      limit: 10,//每页数据记录个数
      state: $scope.UNISSUE//出票订单装填
    };

    var query = $scope.query = function () {//查询订单
      $http.get(url, {params: $scope.queryParams})
        .success(function (data) {
          $scope.records = data.results;
          $scope.totalItems = data.total;
      });
    };

    query();

    /* 查询订单数 */
    function ordersCounts(){
      $http.get(url + '/countBy23States')
        .success(function(data){
          var counts = data.results.split(',');
          $scope.unissueCounts = counts[0];
          $scope.issueingCounts = counts[1];
        });
    }

    ordersCounts();

    /* 切换出票状态 */
    $scope.switch = function(state){
      $scope.queryParams.state = state;
      $scope.queryParams.createTimeBegin = null;
      $scope.queryParams.createTimeEnd = null;
      bzIssue.setState(state);
      query();
    };
  });
