'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('PurBalanceModalCtrl', function ($scope, $http, $modalInstance, bzDicts, item, $filter) {

    $scope.item = item;
    $scope.record = {
      actualMoney: item.collectMoney - item.payMoney,
      //settleTime: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm'),
      //settleTime: (new Date()).getTime(),
      payObjectType: item.payObjectType,
      payObjectId: item.payObjectId,
      orderIds: item.orderIds,
      refundIds: item.refundIds,
      changeIds: item.changeIds,
      detailIds: item.detailIds
    };

    bzDicts.query('PAY_TYPE')//支付方式
      .then(function(data){
        $scope.payTypes = data;
        $scope.record.payType = $scope.payTypes[0].code;
      });

    $scope.settleDiscountBlur = function(){
      $scope.record.settleDiscount = $scope.record.settleDiscount? $scope.record.settleDiscount : 0;
      $scope.record.actualMoney = item.collectMoney - item.payMoney - $scope.record.settleDiscount;
    };

    $scope.save = function(){//保存
      $http.post('api/settlements', $scope.record)
        .success(function(data){
          if(data.success){
            $modalInstance.close(true);
          }else{
            $modalInstance.close(false);
          }
        });
    };

    $scope.cancel = function(){//取消
      $modalInstance.dismiss('cancel');
    }

  });
