'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:NewstaffCtrl
 * @description
 * # NewstaffCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('PasswordResetCtrl', function ($scope, $http) {

    $scope.user = {};
    $scope.sendOK = false;//是否发送成功
    $scope.showAlert = false;

    $scope.send = function(){
      var validator = $('form[name=form]').data('bootstrapValidator');
      validator.validate();
      if(validator.isValid()){
        $http.post('api/users/getTempPassword', $scope.user)
          .success(function(){
            $scope.sendOK = true;
            $scope.showAlert = false;
          })
          .error(function(){
            validator.updateStatus('username', 'INVALID', function(){
              return {
                valid: false,
                message: null
              };
            });
            validator.updateStatus('email', 'INVALID', function(){
              return {
                valid: false,
                message: null
              };
            });
            $scope.showAlert = true;
          });
      }
    };

    $scope.fields = {
      username: {
        validators: {
          notEmpty: {},
          stringLength: {
            max: 20
          }
        }
      },
      email: {
        validators: {
          notEmpty: {},
          emailAddress: {}
        }
      }
    }
  });
