'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:TicketsissuedCtrl
 * @description
 * # TicketsissuedCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('TicketsRefundsCtrl', function ($scope, $http, bzDicts, localStorageService) {

    var UNREFUND = $scope.UNREFUND = '1';//未退票
    var REFUNDING = $scope.REFUNDING = '2';//退票中
    var REFUNDED = $scope.REFUNDED = '3';//已退票
    var REFUND_FAIL = $scope.REFUND_FAIL = '4';//已退票

    bzDicts.query('TICKET_REFUND_STATE')
      .then(function(data){
        $scope.ticketRefundStates = data;
      });

    /**
     * 查询国际退票
     */
    $scope.query = function () {
      $http.get('api/tickets/refunds', {params: $scope.queryParams})
        .success(function (data) {
          $scope.records = data.results;
          $scope.totalItems = data.total;
      });
    };

    $http.get('api/tickets/refunds/counts/state')
      .success(function (data) {
        $scope.counts = data.results;
      });

    /* 切换退票状态 */
    $scope.switch = function(state){

      localStorageService.set('CUR_REFUND_STATE', state);

      //查询条件
      $scope.queryParams = {
        no: 1,//初始页面
        limit: 10,//每页数据记录个数
        state: state? state : UNREFUND,//退票订单状态,
        startTime: null,
        endTime: null
      };
      $scope.query();
    };

    $scope.curState = localStorageService.get('CUR_REFUND_STATE');
    $scope.refunds = {
      UNREFUND: [],
      REFUNDING: [],
      REFUNDED: [],
      'REFUND_FAIL': []
    };

    if($scope.curState === UNREFUND){
      $scope.refunds.UNREFUND.state=true;
    }else if($scope.curState === REFUNDING){
      $scope.refunds.REFUNDING.state=true;
    }else if($scope.curState === REFUNDED){
      $scope.refunds.REFUNDED.state=true;
    }else if($scope.curState === REFUND_FAIL){
      $scope.refunds.REFUND_FAIL.state=true;
    }


  });
