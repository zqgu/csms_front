'use strict';

/**
 * @ngdoc function
 * @name projApp.controller:RoleusersCtrl
 * @description
 * # RoleusersCtrl
 * Controller of the projApp
 */
angular.module('projApp')
  .controller('RoleUsersCtrl', function ($scope, $routeParams, Restangular) {

    $scope.queryParams = {
      limit: 10,
      no: 1
    };

    function queryRole(){
      Restangular.one('roles', $routeParams.id).get()
        .then(function(data){
          $scope.roleName = data.roleName;
        });
    }

    function queryDepts(){
      return Restangular.all('departments').getList($scope.queryParams)
        .then(function(data){
          $scope.depts = data;
        });
    }

    /*查询员工*/
    $scope.queryStaves = function(){
      Restangular.one('roles', $routeParams.id).all('users').getList($scope.queryParams)
        .then(function(data){
          $scope.staves = _.map(data, function(v){
            if(v.deptId){
              var dept = _.find($scope.depts, {id: v.deptId.toString()});
              v.deptName = dept.text;
              return v;
            }else{
              return v;
            }
          });
          $scope.total = data.total;
        });
    };

    queryRole();
    queryDepts().then($scope.queryStaves);

  });
