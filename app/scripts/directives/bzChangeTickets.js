'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzCity
 * @description
 * # bzCity
 */
angular.module('projApp')
  .directive('bzChangeTickets', function (localStorageService) {
    return {
      require: '?ngModel',
      restrict: 'A',
      templateUrl: 'templates/bzChangeTickets.html',
      scope: {
        checkbox: '=?',
        selected: '=?',
        inter: '=?'
      },
      link: function(scope, element, attrs ,ngModel){

        var UNCHANGE = scope.UNCHANGE = '1';//未改签
        var CHANGING = scope.CHANGING = '2';//改签中
        var CHANGED = scope.CHANGED = '3';//已改签
        var CHANGE_FAIL = scope.CHANGE_FAIL = '4';//改签失败

        if(!ngModel){
          return;
        }

        var curChangeState;//判断是国内还是国际改签
        if(scope.inter){
          curChangeState = localStorageService.get('CUR_INTER_CHANGE_STATE');
        }else{
          curChangeState = localStorageService.get('CUR_CHANGE_STATE');
        }

        scope.curState = curChangeState? curChangeState : UNCHANGE;

        ngModel.$formatters.push(function(value){
          scope.changes = value;
        });

        scope.checkboxes = {checked: false, items: {}};

        /*监听全选*/
        scope.$watch('checkboxes.checked', function (value) {
          if(!scope.changes){
            return;
          }
          _.each(scope.changes, function (item) {
            if (angular.isDefined(item.changeId)) {
              scope.checkboxes.items[item.changeId] = value;
            }
          });
        });

        /*监听人员选择*/
        scope.$watch('checkboxes.items', function (e) {
          if (!scope.changes) {
            return;
          }
          var
            checked = 0,
            unchecked = 0,
            isChecked,
            total = scope.changes.length;

          scope.$parent.$parent.selected = [];

          _.each(scope.changes, function (item) {//记录选中和未选中个数
            isChecked = scope.checkboxes.items[item.changeId];
            checked += isChecked || 0;
            unchecked += !isChecked || 0;

            if(isChecked){
              scope.$parent.$parent.selected.push(item);
            }
          });

          if ((unchecked === 0) || (checked === 0)) {
            scope.checkboxes.checked = (checked === total);
          }

          $('#select_all').prop('indeterminate', (checked !== 0 && unchecked !== 0));//是否半选
        }, true);

      }
    };
  });
