'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzNotification
 * @description
 * # bzNotification
 */
angular.module('projApp')
  .filter('fgTimeCost', function ($filter) {
    return function (input) {

      if(!input){
        return ;
      }

      var t = new Date().getTime() - input;

      if(t < 0){
        return ;
      }

      //天
      var d = Math.floor(t / (1000 * 60 * 60 * 24));
      //小时
      var h = Math.floor(t / (1000 * 60 * 60));
      //分钟
      var m = Math.floor(t / (1000 * 60));

      if(m === 0){
        return '刚刚';
      }else if(m > 0 && m < 60){
        return m + '分钟前';
      }else if(h > 0 && h < 24){
        return h + '小时前';
      }else if(h > 24 && d < 365){
        return $filter('date')(input, 'MM月dd日');
      }else if(d > 365){
        return $filter('date')(input, 'yyyy/MM/dd');
      }

      return d + '天' + h + '小时' + m + '分钟';
    };
  })
  .directive('bzNotification', function (bzMessagesService, $interval) {
    return {
      templateUrl: 'templates/bzNotification.html',
      restrict: 'EAC',
      replace: true,
      scope: {},
      link: function postLink(scope, element) {
        var query = function () {
          // 查询未读消息
          /*bzMessagesService.getUnreads().success(function (data) {
            scope.records = data.results;
          });*/
        };

        // query();

        // 定时刷新
        // var stopTime = $interval(query, 1000 * 60);

        element.bind('$destroy', function() {
          // $interval.cancel(stopTime);
        });
      }
    };
  });
