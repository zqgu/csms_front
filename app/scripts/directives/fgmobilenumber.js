'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgMobileNumber
 * @description
 * # fgMobileNumber
 */
angular.module('projApp')
  .directive('fgConfirmValue', function () {
    return {
      restrict: 'AC',
      require: '?ngModel',
      scope: {
        target: '=?'
      },
      link: function postLink (scope, element, attrs, ngModel) {
        if(!ngModel) {
          return;
        }

        element.on('blur', function (){
          scope.$apply(read);
        });

        var read = function () {
          var value = element.val();
          ngModel.$setViewValue(value);
        };

        ngModel.$parsers.push(function (value) {
          if (value === scope.target) {
            ngModel.$setValidity('fgConfirmValue', true);
            return value;
          }else {
            ngModel.$setValidity('fgConfirmValue', false);
          }
        });
      }
    };
  })
  .directive('fgReNameEn', function () {
    return {
      restrict: 'AC',
      require: '?ngModel',
      link: function postLink(scope, element, attrs, ngModel) {
        if(!ngModel){
          return;
        }

        element.on('blur', function () {
          scope.$apply(read);
        });

        var read = function () {
          var value = element.val();
          ngModel.$setViewValue(value);
        };

        ngModel.$parsers.push(function (data) {
          //1，去除 value 的首尾的空格
          var value = data.replace(/^(\s*) | (\s*)$/g, '');
          //如果存在 ‘/’
          if(value.indexOf('/') !== -1){
            var t = value.split(/\//);
            t[0] = t[0].replace(/\s/g, '');
            value = t.join('/');
          }
          element.val(value);

          //如果值为 '' 或 length > 28
          if(value.match(/\w+\/\w+(\s\w+)?/)===null || value.length > 28){
            ngModel.$setValidity('fgReNameEn', false);
          }
          else 
          {
            ngModel.$setValidity('fgReNameEn', true);
          }
          return value;
        });

      }
    };
  })
  .constant('reMobile', /^1\d{10}$/)//简单的手机号匹配正则表达式
  .directive('fgMobile', function (reMobile) {
    return {
      restrict: 'AC',
      require: '?ngModel',
      link: function postLink(scope, element, attrs, ngModel) {
        if(!ngModel){
          return;
        }

        // 监听 input 的变化
        element.on('blur keyup change', function () {
          scope.$apply(read);
        });

        var read = function () {
          var value = element.val();
          ngModel.$setViewValue(value);
        };

        ngModel.$parsers.push(function (value){
          if(value && value.length > 11){
            value = value.slice(0,11);
            element.val(value);
          }

          //正则判断是否合法
          var bool = reMobile.test(value);

          if (bool){
            ngModel.$setValidity('formatError', true);
            return value.replace(/-/g, '');
          } else {
            ngModel.$setValidity('formatError', false);
          }
        });

      }
    };
  });
