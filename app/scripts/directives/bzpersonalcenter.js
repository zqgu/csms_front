'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzPersonalCenter
 * @description
 * # bzPersonalCenter
 */
angular.module('projApp')
  .directive('bzPersonalCenter', function ($location) {
    return {
      templateUrl: 'templates/bzPersonalCenter.html',
      restrict: 'EAC',
      replace: true,
      scope: {},
      link: function postLink(scope, element) {
        element.find('a[ng-href]').on('click', function (){
          var href = $(this).attr('ng-href').replace(/#/, '');
          $location.path(href);
        });
      }
    };
  });
