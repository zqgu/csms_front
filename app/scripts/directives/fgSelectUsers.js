'use strict';

/**
 * @ngdoc directive
 * @name wsdcApp.directive:fgAppNavbar
 * @description
 * # fgAppNavbar
 */

angular.module('projApp')
  .controller('FgSelectUsersCtrl', function($scope, $modalInstance, $http, items){

    $scope.multi = items.multi;

    $scope.queryParams = {
      no: 1,
      limit: 9,
      privCode: items.priCode
    };

    $scope.selectedUsers = items;

    var query = $scope.query = function(){//查询人员
      $http.get('api/users', {params: $scope.queryParams})
        .success(function(data){
          $scope.users = _.map(data.results, function(v){
            return {id: v.userId, code: v.employeeNo, name: v.nameCn}
          });
          $scope.total = data.total;

          if($scope.multi){
            $('#select_all').prop('indeterminate', ($scope.selectedUsers.length !==0 && $scope.selectedUsers.length !== $scope.users.length));//是否半选
          }else{
            if($scope.selectedUsers.length > 0){
              $scope.selectedUser = _.find($scope.users, {id: $scope.selectedUsers[0].id});
            }
          }
        })
    };

    query();

    if($scope.multi){//多选人员

      $scope.checkboxes = {checked: false, items: {}};

      /*监听全选*/
      $scope.$watch('checkboxes.checked', function (value) {
        _.each($scope.users, function (item) {
          if (angular.isDefined(item.id)) {
            $scope.checkboxes.items[item.id] = value;
          }
        });
      });

      /*监听人员选择*/
      $scope.$watch('checkboxes.items', function (newValue, oldValve) {
        if (!$scope.users) {
          return;
        }
        var
          checked = 0,
          unchecked = 0,
          tmpUsers = [],//当前选中的checkbox
          isChecked,
          total = $scope.users.length;

        _.each($scope.users, function (item) {//将选中的人员加入到tmpUsers中,并且记录选中和未选中个数
          isChecked = $scope.checkboxes.items[item.id];
          if(isChecked){
            tmpUsers.push(item);
          }
          checked += isChecked || 0;
          unchecked += !isChecked || 0;
        });

        for(var v in newValue){
          var i = _.findIndex($scope.selectedUsers, {id: parseInt(v)});
          if(i !== -1 && !newValue[v]){//删除元素
            $scope.selectedUsers.splice(i, 1);
          }else if(i === -1 && newValue[v]){//添加元素
            var user = _.find($scope.users, {id: parseInt(v)});
            $scope.selectedUsers.push(user);
          }
        }

        if ((unchecked === 0) || (checked === 0)) {
          $scope.checkboxes.checked = (checked === total);
        }

        $('#select_all').prop('indeterminate', (checked !== 0 && unchecked !== 0));//是否半选
      }, true);

      $scope.$watchCollection('selectedUsers', function(newArr){//监听已选人员变化
        for(var v in $scope.checkboxes.items){//将所有checkbox置为false
          $scope.checkboxes.items[v] = false;
        }

        if(angular.isArray(newArr) && newArr.length > 0){//根据selectedUsers选中checkbox
          _.each(newArr, function(v){
            $scope.checkboxes.items[v.id] = true;

          });
        }
      });
    }else{//单选人员
      $scope.$watch('selectedUser', function(value){//监听已选人员变化
        if(!value){
          return;
        }
        $scope.selectedUsers[0] = $scope.selectedUser;
      }, true);
    }


    $scope.save = function(){//保存
      $modalInstance.close($scope.selectedUsers);
    };

    $scope.cancel = function(){//取消
      $modalInstance.dismiss('cancel');
    };

  });

angular.module('projApp')
  .directive('fgSelectUsers', function ($modal) {
    return {
      restrict: 'A',
      require: '?ngModel',
      scope: {},
      link: function postLink(scope, element, attrs, ngModel) {

        if(!ngModel){
          return ;
        }

        element.tagsinput({//初始化组件
          itemValue: 'id',
          itemText: 'name',
          freeInput: false,
          maxTags: attrs['maxUsers']? attrs['maxUsers'] : null
        });

        $('.bootstrap-tagsinput').addClass('form-control').css({'margin-bottom': 0});//调整样式

        scope.$watch(//ngModel无法深度监听对象数组
          function() {
            return ngModel.$modelValue;
          }, function(modelValue) {

            if(typeof modelValue  === 'undefined'){
              if(!attrs.hasOwnProperty('single')){
                ngModel.$setViewValue([]);
              }else{
                ngModel.$setViewValue({});
              }
            }

            var copyModelValue = angular.copy(modelValue);

            element.tagsinput('removeAll');//removeAll()后，modelValue为空

            if(angular.isArray(copyModelValue)){
              _.each(copyModelValue, function(v){
                if(!v.id|| !v.name){//不添加空对象
                  return ;
                }
                element.tagsinput('add', v);
              })
            }else if(angular.isObject(copyModelValue)){
              if(!copyModelValue.id|| !copyModelValue.name){//不添加空对象
                return ;
              }
              element.tagsinput('add', copyModelValue);
            }

          }, true);

        ngModel.$parsers.push(function(value){//好混乱
          if(!attrs.hasOwnProperty('single')){
            if(angular.isArray(value)){
              return value;
            }else{
              return element.tagsinput('items');
            }
          }else{
            if(angular.isArray(value)){
              return value[0];
            }else if(angular.isString(value) && element.tagsinput('items').length > 0){
              return element.tagsinput('items')[0];
            }else if(value === ''){
              return {};
            }
            return value;
          }

        });

        var input = element.tagsinput('input');
        element.next().on('click focus', function(e){//监听，弹出人员选择模态框
          if($(e.target).is('span')){//当点击tags不弹出模态框
            return;
          }

          var modalInstance = $modal.open({
            templateUrl: 'templates/fgSelectUsers.html',
            controller: 'FgSelectUsersCtrl',
            resolve: {
              items: function () {
                var multi = !attrs.hasOwnProperty('single');
                var arr = [];
                if(!ngModel.$modelValue){
                  arr.multi = multi;
                  arr.pri = attrs.pri;
                }
                ngModel.$modelValue.multi = multi;
                ngModel.$modelValue.priCode = attrs.priCode;
                return ngModel.$modelValue;
              }
            }
          });

          modalInstance.result.then(function (selectedItem) {//返回选中人员
            ngModel.$setViewValue(selectedItem);
          });

        });

      }
    };
  });
