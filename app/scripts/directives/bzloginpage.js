'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzLoginPage
 * @description
 * # bzLoginPage
 */
angular.module('projApp')
  .directive('bzLoginPage', function (fgAuthenticate, $cookies, $rootScope) {
    return {
      templateUrl: 'templates/bzLoginPage.html',
      restrict: 'EA',
      scope: {},
      link: function postLink(scope) {
        scope.user = {};

        scope.login = function (){
          fgAuthenticate.login(scope.user)
            .error(function(data){
              scope.showAlert = true;
              scope.errMsg = data.customMsg;
            });
        };

        if ($cookies.username) {
          scope.rememberMe = true;
          scope.user.username = $cookies.username;
        }

        scope.$watch('rememberMe', function (){
          if(scope.user.username && scope.rememberMe){
            $cookies.username = scope.user.username;
          }else{
            $cookies.username = '';
          }
        });

        scope.$watch('user', function (){
          if(scope.rememberMe){
            $cookies.username = scope.user.username;
          }
        },true);

        scope.forget = function(){
          $rootScope.selection = 'passwordReset';
        };

      }
    };
  });
