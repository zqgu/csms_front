'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgSimpleTree
 * @description
 * # fgSimpleTree
 */
angular.module('projApp')
  .constant('treejson', [{
    id: 1,
    name: '一级菜单 A'
  },{
    id: 2,
    name: '一级菜单 B'
  },{
    id: 3,
    pid: 2,
    name: '二级菜单 A'
  },{
    id: 4,
    pid: 2,
    name: '二级菜单 B'
  },{
    id: 5,
    pid: 4,
    name: '三级菜单 A'
  }])
  .constant('treeDefaultOptions', {
    id: 'id',
    pid: 'pid',
    name: 'name'
  })
  .directive('fgSimpleTree', function ($compile) {
    return {
      template: '<ul></ul>',
      restrict: 'EAC',
      scope: {
        data: '=?'
      },
      replace: true,
      link: function postLink(scope, element, attrs) {

        scope.$watch('data', onDataChange);

        //根据pid,查询子节点
        var findSubNode = function (pid) {
          return _.filter(scope.data, function (node) {
            return node.pid === pid;
          })
        };

        var $root = $(element).on('click', 'li a', function (event){
          if($(this).data('data')){
            var $ul = $(this).parent('li').find('ul');
            $ul.toggle();
            event.stopPropagation();
            return;
          }

          var id = $(this).data('id');
          var nodes = findSubNode(id);
          if (nodes.length > 0) {
            var child = scope.$new(true);
            child.nodes = nodes;
            $(this).after($compile($('<div fg-simple-tree data="nodes" pid="' + id + '"/>'))(child));
            $(this).data('data', nodes);
          }
        });

        var onDataChange = function (nodes) {
          if(!(nodes && angular.isArray(nodes))){
            return;
          }

          for(var i=0,size=nodes.length;i<size;i++) {
            var node = nodes[i];
            if(node.pid + '' === $root.attr('pid') + '') {
              var $label = $('<a>').text(node.name).data('id', node.id);
              var $node = $('<li>').append($label);
              $root.append($node);
            }
          }
        };

        if(scope.data) {
          onDataChange(scope.data);
        }

      }
    };
  });
