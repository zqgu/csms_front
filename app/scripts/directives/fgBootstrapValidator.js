'use strict';

angular.module('projApp')
  .directive('fgBootstrapValidator', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        var
            fields = scope.$eval(attrs.fgBootstrapValidator),
            validatorOption = {
            message: '这个值校验不通过',//默认错误提示
            feedbackIcons: {//校验图标
              valid: 'glyphicon glyphicon-ok',
              invalid: 'glyphicon glyphicon-remove',
              validating: 'glyphicon glyphicon-refresh'
            },
            fields: fields,
            submitButtons:'button[name=submit]',//表单校验通过前无法点击
            container: null,//可选值popover、tooltip
            include: ':disabled, :hidden, :not(:visible)',//判定哪些字段不必校验
            group: '.form-group',//默认字段的父节点选择器
            live: 'enabled',//实时校验
            threshold: null,//少于一定值前不做实时校验
            trigger: 'blur',//字段触发校验事件
            verbose: true,//
            onSuccess: function(e){},
            onError: function(e){}
          };

        $(element).bootstrapValidator(validatorOption)
          .on('submit.bv', function(e){
            console.debug(e);
          });
          /*.on('error.field.bv', function(e, data){//控制error message显示方式
            var
              formControl = $(e.target),
              messages = formControl.parent().find('.help-block');
            _.each(messages, function(v){
              $(v).css({'margin-top': '0', height: '15px'});
            });
            //formControl.parent().css({'margin-top': 0});
            if(formControl.parent().parent().height() === 34){
              formControl.parent().parent().css({'margin-bottom': '0'});
            }
            if(formControl.parent().parent().height() === 49){
              formControl.parent().parent().css({'margin-bottom': '15'});
            }
          })
          .on('success.field.bv', function(e, data){
            var formControl = $(e.target);
            formControl.parent().parent().parent().children().css({'margin-bottom': 15});
          });*/

      }
    };
  });
