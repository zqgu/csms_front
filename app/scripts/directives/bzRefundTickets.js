'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzCity
 * @description
 * # bzCity
 */
angular.module('projApp')
  .directive('bzRefundTickets', function (localStorageService) {
    return {
      require: '?ngModel',
      restrict: 'A',
      templateUrl: 'templates/bzRefundTickets.html',
      scope: {
        checkbox: '=?',
        selected: '=?'
      },
      link: function(scope, element, attrs ,ngModel){

        var UNREFUND = scope.UNREFUND = '1';//未退票
        var REFUNDING = scope.REFUNDING = '2';//退票中
        var REFUNDED = scope.REFUNDED = '3';//已退票
        var REFUND_FAIL = scope.REFUND_FAIL = '4';//退票失败

        if(!ngModel){
          return;
        }

        var curRefundState;
        if(scope.inter){
          curRefundState = localStorageService.get('CUR_INTER_REFUND_STATE');
        }else{
          curRefundState = localStorageService.get('CUR_REFUND_STATE');
        }
        scope.curState = curRefundState? curRefundState : UNREFUND;

        ngModel.$formatters.push(function(value){
          scope.refunds = value;
        });

        scope.checkboxes = {checked: false, items: {}};

        /*监听全选*/
        scope.$watch('checkboxes.checked', function (value) {
          if(!scope.refunds){
            return;
          }
          _.each(scope.refunds, function (item) {
            if (angular.isDefined(item.refundId)) {
              scope.checkboxes.items[item.refundId] = value;
            }
          });
        });

        /*监听人员选择*/
        scope.$watch('checkboxes.items', function (e) {
          if (!scope.refunds) {
            return;
          }
          var
            checked = 0,
            unchecked = 0,
            isChecked,
            total = scope.refunds.length;

          scope.$parent.$parent.selected = [];

          _.each(scope.refunds, function (item) {//记录选中和未选中个数
            isChecked = scope.checkboxes.items[item.refundId];
            checked += isChecked || 0;
            unchecked += !isChecked || 0;

            if(isChecked){
              scope.$parent.$parent.selected.push(item);
            }
          });

          if ((unchecked === 0) || (checked === 0)) {
            scope.checkboxes.checked = (checked === total);
          }

          $('#select_all').prop('indeterminate', (checked !== 0 && unchecked !== 0));//是否半选
        }, true);

      }
    };
  });
