'use strict';

/**
 * @ngdoc directive
 * @name wsdcApp.directive:fgAppNavbar
 * @description
 * # fgAppNavbar
 * 用于表单中的部门选择
 */
angular.module('projApp')
  .directive('fgDeptRead', function ($http, $compile) {
    return {
      restrict: 'A',
      require: '?ngModel',
      scope: {},
      link: function postLink(scope, element, attrs, ngModel) {

        var depts = [];
        //var selectedNode;
        //scope.status = false;

        var $dropdown = $('<div class="dropdown"/>');
        var $dropdownMenu = $('<ul class="dropdown-menu" id="deptTree" style="overflow-x: hidden; overflow-y: auto; max-height:300px; width: 230px"/>');

        $(element)
          .attr("data-toggle", "dropdown")
          .wrap($dropdown)
          .after($dropdownMenu);

        if(!ngModel){
          return;
        }

        ngModel.$parsers.push(function(v){
          if(!v){
            return;
          }
          return v.id;
        });

        ngModel.$formatters.push(function(model){
          if(!model || depts.length === 0){
            return;
          }
          return _.find(depts, {id: model.toString()}).text;
        });

        $http.get('api/departments')
          .success(function(data){
            depts = data.results;
            ngModel.$modelValue = '';

            $('#deptTree').jstree({//初始化部门树
              core : {
                data: depts,
                multiple: false
              },
              plugins : [ 'wholerow' ]
            });

            $('#deptTree')
              .on('open_node.jstree', function(e, data){//监听节点打开事件
                data.instance.set_icon(data.node, "fa fa-folder-open");
              })
              .on('close_node.jstree', function(e, data){//监听节点关闭事件
                data.instance.set_icon(data.node, "fa fa-folder");
              })
              .on('select_node.jstree', function(e, data){//监听节点选择事件
                //scope.status = !scope.status;
                //$('div.dropdown').dropdown('toggle');
                ngModel.$setViewValue(data.node);
                ngModel.$modelValue = '';
                scope.$apply();
                /*if(selectedNode && selectedNode.id === data.node.id){//取消选中节点
                  $('#deptTree').jstree(true).deselect_node(data.node);
                  selectedNode = null;
                  ngModel.$setViewValue({});
                  ngModel.$modelValue = '';
                  scope.$apply();
                }else{
                  selectedNode = data.node;
                  ngModel.$setViewValue(data.node);
                  ngModel.$modelValue = '';
                  scope.$apply();
                }*/

              });
          });
      }
    };
  });
