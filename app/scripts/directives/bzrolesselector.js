'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzRolesSelector
 * @description
 * # bzRolesSelector
 */
angular.module('projApp')
  .directive('bzRolesSelector', function ($http) {
    return {
      templateUrl: 'templates/bzRolesSelector.html',
      restrict: 'EAC',
      replace: true,
      require: '?ngModel',
      link: function postLink(scope, element, attrs, ngModel) {
        if (!ngModel){
          return;
        }



        scope.$watchCollection('selects', function (selects) {
          if(!angular.isArray(selects)){
            return;
          }

          var values = [];

          for(var i = 0, size = selects.length; i < size; i++){
            var value = selects[i];
            if (value){
              values.push(selects[i]);
            }
          }

          ngModel.$setViewValue(values.join(','));
        });

        ngModel.$parsers.push(function (value){
          return value;
        });

        ngModel.$formatters.push(function (model){
          $http.get('api/roles').success(function (data){
            scope.roles = data.results;
            scope.selects = new Array(scope.roles.length);

            if (!ngModel.$modelValue) {
              return;
            }

            var values = ngModel.$modelValue.split(',');

            for (var i = 0, size = scope.roles.length; i < size; i++){
              for (var j = 0, len = values.length; j < len; j++) {
                if (values[j] === scope.roles[i].roleId.toString()) {
                  scope.selects[i] = values[j];
                }
              }
            }
          });
          return model;
        });
      }
    };
  });
