'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzPermissionSelector
 * @description
 * # bzPermissionSelector
 */
angular.module('projApp')
  .directive('bzPermissionSelector', function ($http, $routeParams, bzPris) {
    return {
      templateUrl: 'templates/bzPermissionSelector.html',
      restrict: 'EAC',
      replace: true,
      require: '?ngModel',
      scope: {},
      link: function postLink(scope, element, attrs, ngModel) {
        if (!ngModel){
          return;
        }

        scope.$watchCollection('selects', function (selects) {
          if(!angular.isArray(selects)){
            return;
          }

          var values = [];

          for(var i = 0, size = selects.length; i < size; i++){
            var value = selects[i];
            if (value){
              values.push(selects[i]);
            }
          }

          ngModel.$setViewValue(values.join(','));
        });

        ngModel.$parsers.push(function (value){
          return value;
        });

        ngModel.$formatters.push(function (model){
          bzPris.query($routeParams.id).then(function (data){
            scope.privs = data;
            scope.selects = new Array(scope.privs.length);

            if (!ngModel.$modelValue) {
              return;
            }

            var values = ngModel.$modelValue.split(',');

            for (var i = 0, size = scope.privs.length; i < size; i++){
              for (var j = 0, len = values.length; j < len; j++) {
                if (values[j] === scope.privs[i].privId.toString()) {
                  scope.selects[i] = values[j];
                }
              }
            }
          });

          return model;
        });

      }
    };
  });
