'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgSelect
 * @description
 * # fgSelect
 */
angular.module('projApp')
  .directive('fgSelect', function ($rootScope) {
    var linker = function postLink(scope, element, attrs, ngModel) {

        scope.init = function(){
          $(element).empty();
          $(element).multiselect('destroy');
          //遍历数据创建option
          angular.forEach(scope.records,function(e){
              //数据为字符串，option的value和html值为字符串
              if(_.isString(e)){
                var value = e;
                var label = e;
              }

              //数据为对象，则为选中的key值
              if(_.isObject(e)){
                var value = e[scope.optionValue] || e[scope.optionLabel];
                var label = e[scope.optionLabel] || e[scope.optionValue];
              }

              var opt = $('<option>').attr('value',value)
                                    .data("objs", e)
                                    .html(label)
                                    .appendTo(element);

          });

          //初始化组件
          $(element).multiselect({
            buttonClass: 'btn btn-default btn-sm',
            disableIfEmpty: true,
            buttonText: function(options, select) {
              return scope.buttonText || 'default';
            },

            buttonTitle: function(options, select) {
              var labels = [];
              options.each(function () {
              labels.push($(this).text());
              });
              return labels.join(' - ');
            },
            onChange: function (option, checked, select){
              scope.selectedValue = option.data('objs');
              $rootScope.$broadcast('fgSelect:change');
              $rootScope.$apply();
            }
          });

          //勾选初始值
          //$('.jk-select').multiselect('select', ['1', '2',]);
        };

        //监控records
        scope.$watchCollection('records',function(records){

          //无数据则隐藏select，有数据执行 multiselect 时会做隐藏操作
          if(!scope.records){
            $(element).css('display','none');
          }

          if(_.isArray(scope.records) && scope.records.length > 0){
            //console.log(scope.records);
            scope.init();
          }
        });

    };

    return {
      restrict: 'EAC',
      replace : true, 
      scope: {
        records:'=?',
        selectedValue:'=?',
        buttonText:'@',
        optionLabel:'@',
        optionValue:'@',
        selectOnChange:'&'
      },
      link: linker
    };

  });
