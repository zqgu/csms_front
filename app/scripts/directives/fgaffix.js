'use strict';

/**
 * @ngdoc directive
 * @name fgAffixApp.directive:fgAffix
 * @description
 * # fgAffix
 */
angular.module('projApp')
  .directive('fgAffix', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element) {

        var $element = $(element);

        //监听 scroll 事件
        $(document).on('scroll', function () {
          //获取滚动距离
          var top = $('body').scrollTop();
          //记录是否处于 affix 状态
          var affixed = $element.hasClass('affix');
          
          //当滚动距离大于 30 && 不处于 affix 状态时
          if(top >= 30 && !affixed) {
            //添加 class affix 
            $element.addClass('affix');
          }
          //当滚动距离小于30 && 处于 affix 状态时
          else if(top < 30 && affixed){
            $element.removeClass('affix');
          }
        });

      }
    };
  });
