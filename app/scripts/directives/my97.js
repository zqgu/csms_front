// 'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:my97
 * @description
 * # my97 datepicker
 @example
 <example module="projApp">
 <file name="index.html">
 <textarea ng-model="text" r-autogrow class="input-block-level"></textarea>
 <pre>{{text}}</pre>
 </file>
 </example>
 */
angular.module('projApp')
  .directive('datePicker', function () {
    return {
      restrict: 'A',
      require: '?ngModel',
      scope: {
        minDate: '@?',
        maxDate: '@?'
      },
      link: function (scope, element, attrs, ngModel) {
        if(!ngModel){
          return;
        }

        var onpickingHandler = function (dp) {
          var newDate = dp.cal.getNewDateStr();
          ngModel.$setViewValue(newDate);
          scope.$apply();
        };

        var singleCalendar = attrs.singleCalendar === 'true';

        var options = {
          onpicking: onpickingHandler,
          el: element.get(0), //必须
          doubleCalendar: !singleCalendar, //双日历
          dateFmt: 'yyyy-MM-dd', //时间格式
          oncleared: function(){//清除时间
            ngModel.$setViewValue(null);
          }
        };

        var WdatePicker = function () {
          if(scope.minDate){

            if(scope.minDate === 'today'){
              scope.minDate = new Date();
            }

            angular.extend(options, {minDate: scope.minDate});
          }

          if(scope.maxDate) {
            angular.extend(options, {maxDate: scope.maxDate});
          }

          window.WdatePicker(options);
        };

        element.on('click focus', WdatePicker);

        //directive 销毁时去除监听
        element.bind('$destroy', function (){
          element.unbind('click', WdatePicker);
        });
      }
    };
  });
