'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzContactsSelector
 * @description
 * # bzContactsSelector
 */
angular.module('projApp')
  .directive('bzContactsSelector', function ($compile, bzContactsService) {
    return {
      restrict: 'A',
      require: '?ngModel',
      scope: {
        user: '=?'
      },
      link: function postLink(scope, element, attrs, ngModel) {
        if (!ngModel){
          return;
        }
        var $dropdown = $('<div class="dropdown"/>');
        var $dropdownMenu = $('<ul class="dropdown-menu" ng-include="\'templates/bzStavesSelectorTemplate.html\'"/>');

        $(element)
          .attr('data-toggle', 'dropdown')
          .wrap($dropdown)
          .after($compile($dropdownMenu)(scope));

        $dropdown.dropdown();

        element.on('keyup click', function (){
          //调用员工查询 service:bzContactsService
          var promise = bzContactsService.query(/*element.val()*/);

          //返回的是一个 promise 对象
          promise.then(function (contacts) {
            scope.contacts = contacts;
          }); 
        });

        scope.onclick = function ($event, contact) {
          scope.user = contact;
          console.log(contact);
          $(element).val(contact.nameCn);
          ngModel.$setViewValue(contact.nameCn);
        };
        
      }
    };
  });
