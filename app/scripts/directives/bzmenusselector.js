'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzMenusSelector
 * @description
 * # bzMenusSelector
 */
angular.module('projApp')
  .directive('bzMenusSelector', function ($http, bzmenus) {
    return {
      templateUrl: 'templates/bzMenusSelector.html',
      restrict: 'EAC',
      replace: true,
      require: '?ngModel',
      scope: {},
      link: function postLink(scope, element, attrs, ngModel) {
        if (!ngModel){
          return;
        }

        var menus = [];//所有菜单
        scope.menus1 = []; scope.menus2 = [];//分成两类菜单：标题菜单和除了标题菜单的功能模块
        scope.items1 = {}; scope.items2 = {};

        bzmenus.query().then(function (data){//查询菜单
          menus = data;

          _.each(menus, function(v){
            if(v.parentId !== -1){
              scope.menus1.push(v);
            }else{
              scope.menus2.push(v);
            }
          });

          ngModel.$modelValue = '';

        });

        scope.$watch('items1', function(){//监听菜单列表
          listenCheckbox();
        }, true);

        scope.$watch('items2', function(){//监听模块列表
          listenCheckbox();
        }, true);

        function listenCheckbox(){//将选中项添加到selectedMenus中
          var selectedMenus = [];

          _.each(scope.menus1, function(v){
            if(scope.items1[v.menuId]){
              selectedMenus.push(v);
            }
          });

          _.each(scope.menus2, function(v){
            if(scope.items2[v.menuId]){
              selectedMenus.push(v);
            }
          });

          ngModel.$setViewValue(_.pluck(selectedMenus, 'menuId').join(','));
        }

        ngModel.$formatters.push(function (model){

          if(!model || !menus){
            return ;
          }

          var selectedMenus = model.split(',');

          _.each(menus, function(m){
            _.each(selectedMenus, function(v){
              if(m.menuId.toString() === v && m.parentId !== -1){//模块
                scope.items1[v] = true;
              }else if(m.menuId.toString() === v && m.parentId === -1){//菜单
                scope.items2[v] = true;
              }
            });
          });

          ngModel.$setPristine();
          return model;
        });
      }
    };
  });
