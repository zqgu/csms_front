'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgFormControl
 * @description
 * # copy from mcasimir/mobile-angular-ui
 */
angular.module('projApp')
  .directive('fgFormControl', function() {
    var bsColClasses = {};
    var bsColSizes = ['xs', 'sm', 'md', 'lg'];
    
    for (var i = 0; i < bsColSizes.length; i++) {
      for (var j = 1; j <= 12; j++) {
        bsColClasses['col-' + bsColSizes[i] + '-' + j] = true;
      }
    }
    
    function separeBsColClasses(clss) {
      var intersection = '';
      var difference = '';

      for (var i = 0; i < clss.length; i++) {
        var v = clss[i];
        if (v in bsColClasses) { 
          intersection += (v + ' '); 
        } else {
          difference += (v + ' ');
        }
      }

      return {i: intersection.trim(), d: difference.trim()};
    }

    return {
      replace: true,
      require: '?ngModel',
      link: function(scope, elem, attrs) {

        if (attrs.labelClass === null) {
          attrs.labelClass = '';
        }

        if (attrs.id === undefined && attrs.ngModel) {
          attrs.name = attrs.ngModel.replace('.', '_');
          attrs.id = attrs.name + '_input';
        }
        
        if ((elem[0].tagName === 'SELECT') || ((elem[0].tagName === 'INPUT' || elem[0].tagName === 'TEXTAREA') && (attrs.type !== 'checkbox' && attrs.type !== 'radio'))) {
          elem.addClass('form-control');
        }

        var label = angular.element('<label for=\'' + attrs.id + '\' class=\'control-label\'>' + attrs.label + '</label>');
        var w1 = angular.element('<div class=\'form-group row\'></div>'); 
        var w2 = angular.element('<div class=\'form-control-wrapper\'></div>');
        
        var labelColClasses = separeBsColClasses(attrs.labelClass.split(/\s+/));
        if (labelColClasses.i === '') {
          label.addClass('col-xs-12');
        }
        label.addClass(attrs.labelClass);

        var elemColClasses = separeBsColClasses(elem[0].className.split(/\s+/));
        elem.removeClass(elemColClasses.i);
        w2.addClass(elemColClasses.i);
        if (elemColClasses.i === '') {
          w2.addClass('col-xs-12');
        }
        
        var labelWrap = angular.element('<label>');
        var checkboxWrap = angular.element('<div class="checkbox">');
        
        if (attrs.type === 'checkbox'){
          elem.wrap(w1).wrap(w2).wrap(checkboxWrap).wrap(labelWrap);
          elem.parent().parent().parent().parent().prepend(label);
          elem.after('是');
        }else{
          elem.wrap(w1).wrap(w2);  
          elem.parent().parent().prepend(label);
        }

        elem.attr('id', attrs.id);
        elem.attr('name', attrs.name);

        label = w1 = w2 = labelColClasses = elemColClasses = null;
      }
    };
  });
