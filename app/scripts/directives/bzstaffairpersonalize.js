'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzStaffAirPersonalize
 * @description
 * # bzStaffAirPersonalize
 */
angular.module('projApp')
  .directive('bzStaffAirPersonalize', function () {
    return {
      templateUrl: 'templates/bzStaffAirPersonalize.html',
      restrict: 'EAC',
      replace: true
    };
  });
