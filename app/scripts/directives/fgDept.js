'use strict';

/**
 * @ngdoc directive
 * @name wsdcApp.directive:fgAppNavbar
 * @description
 * # fgAppNavbar
 * 用于员工列表页面
 */
angular.module('projApp')
  .directive('fgDept', function (Restangular, $location) {

    function postLink(scope, element, attrs) {

      Restangular.all('departments').getList()
        .then(function(data){
          $('#deptTree').jstree({
            core : {
              data: data,
              multiple: false
            },
            plugins : [ 'wholerow' ]
          });
          $('#deptTree')
            .on('open_node.jstree', function(e, data){//监听节点打开事件
              data.instance.set_icon(data.node, "fa fa-folder-open");
            })
            .on('close_node.jstree', function(e, data){//监听节点关闭事件
              data.instance.set_icon(data.node, "fa fa-folder");
            })
            .on('select_node.jstree', function(e, data){//监听节点选择事件
              scope.selectedNode = data.node;
              scope.$apply();
            })
            .on('deselect_node.jstree', function(e, data){//监听取消节点选择事件
              scope.selectedNode = null;
              scope.$apply();
            });
        });

      scope.edit = function(){
        if(!scope.selectedNode){
          return alert('请选择部门！');
        }
        $location.path('/departments/' + scope.selectedNode.id);
      };

      $('#delBtn').confirmation({
        placement: 'left',
        title: '确定删除？',
        btnOkLabel: '确定',
        btnCancelLabel: '取消',
        onConfirm: function(event){
          event.preventDefault();
          if(scope.selectedNode){
            Restangular.one('departments', scope.selectedNode.id).remove()
              .then(function(){
                delNode();
              })
          }
        },
        onShow: function(event){
          if(!scope.selectedNode){
            event.preventDefault();
            event.stopPropagation();
            return alert('请选择部门！');
          }
        }
      });

      function delNode(){//删除节点
        //$('#deptTree').jstree('remove', scope.selectedNode.id);
        Restangular.all('departments').getList()
          .then(function(data){
            $('#deptTree').jstree(true).settings.core.data = data;
            $('#deptTree').jstree(true).refresh();
            scope.selectedNode = null;
          });
      }

    }

    return {
      restrict: 'A',
      require: '?ngModel',
      replace: true,
      templateUrl: 'templates/bzDept.html',
      scope: {
        selectedNode: '=?'
      },
      link: postLink
    };
  });
