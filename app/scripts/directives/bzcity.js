'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzCity
 * @description
 * # bzCity
 */
angular.module('projApp')
  .directive('bzCity', function (fgCityService, $compile) {

    var linker = function(scope, element, attrs ,ngModel){
      if (!ngModel){
        return;
      }

      var $dropdown = $('<div class="dropdown bz-city-dropdown"/>');
      var $dropdownMenu = $('<ul class="dropdown-menu" ng-include="\'templates/bzCity.html\'"/>');

      $(element)
        .attr('data-toggle', 'dropdown')
        .wrap($dropdown)
        .after($compile($dropdownMenu)(scope));

      $dropdown.dropdown();

      scope.cities = [];

      //设置当前城市
      scope.setCity = function(c,$event){
        if(!c.Airport){
          $event.stopPropagation();
          return;
        }
        ngModel.$setViewValue(c);
        //城市组件通过 text 属性向外暴露当前选择城市对象
        scope.city = c;
        element.val(c.CityName);
      };


      ngModel.$parsers.push(function (value){
        //如果包含 CityCode，说明数据从 scope.setCity 过来的
        if(angular.isObject(value) && value.CityCode){
          ngModel.$setValidity('noThisCity', true);
          return value.CityCode;
        }

        scope.cities = fgCityService.matchedCity(value);
        if(scope.cities){
          scope.pageNum = Math.floor(scope.cities.length/scope.pageSize + 1);
        }
        //scope.cities在 keyup事件处理里被赋值
        //根据输入框查询不出城市时
        if (scope.cities.length === 0) {
          ngModel.$setValidity('noThisCity', false);
        }else{
          ngModel.$setValidity('noThisCity', true);
          return value;
        }
        
      });

      ngModel.$render = function() {
        var city = fgCityService.getCityByCode(ngModel.$modelValue);
        if(city){
          $(element).val(city.CityName);
          //城市组件通过 text 属性向外暴露当前选择城市对象
          scope.city = city;
        }
      };

      scope.page = 1; //当前页码
      scope.pageSize = 10; //每页数据数
      scope.pageNum = Math.floor(scope.cities.length/scope.pageSize); //总页数

      //上一页
      scope.pre  = function($event){
        if(scope.page > 1){scope.page --;}
        $event.stopPropagation();
        scope.status = true;
      };

      //下一页
      scope.next  = function($event){
        if(scope.page < scope.pageNum){scope.page ++;}
        $event.stopPropagation();
        scope.status = true;
      };

      //阻止冒泡事件
      scope.stopPro = function  ($event) {
        $event.stopPropagation();
      };

      //根据首字母查询，如abcdef，xyz，
      scope.changeCitesGroup = function ($event, keys){
        $event.stopPropagation();
        scope.citiesGroup = fgCityService.getCitiesBySzm(keys);
      };

      //热门城市
      scope.getHotCities = function ($event) {
        if ($event) {
          $event.stopPropagation();
        }
        scope.citiesGroup = fgCityService.getHotCities();
      };

      //初始读取热门城市
      scope.getHotCities();
    };

    return {
      // templateUrl: 'templates/bzCity.html',
      require: '?ngModel',
      restrict: 'A',
      replace:true,
      link: linker,
      scope:{
        city: '=?'
      }
    };
  });
