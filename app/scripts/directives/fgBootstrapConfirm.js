'use strict';

angular.module('projApp')
  .directive('fgBootstrapConfirm', function () {
    return {
      restrict: 'A',
      link: function postLink(scope, element, attrs) {
        var confirmOption = scope.$eval(attrs.fgBootstrapConfirm);
        if(!confirmOption.placement){
          confirmOption.placement = 'left';
        }
        if(!confirmOption.title){
          confirmOption.title = '确定删除？';
        }
        if(!confirmOption.btnOkLabel){
          confirmOption.btnOkLabel = '确定';
        }
        if(!confirmOption.btnCancelLabel){
          confirmOption.btnCancelLabel = '取消';
        }

        $(element).confirmation(confirmOption);
      }
    };
  });
