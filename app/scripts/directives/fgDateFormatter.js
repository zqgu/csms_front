'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgFormControl
 * @description
 * # copy from mcasimir/mobile-angular-ui
 */
angular.module('projApp')
  .directive('fgDateFormatter', function($filter) {
    return {
      require: '?ngModel',
      link: function(scope, element, attrs, ngModel){

        if(!ngModel){
          return;
        }

        if(!attrs.fgDateFormatter){
          return;
        }

        element.on('blur keyup change', function(){
          ngModel.$setViewValue(element.val());
        });

        ngModel.$parsers.push(function(v){
          return new Date(v.replace(/\-/g, '/'));
        });

        ngModel.$formatters.push(function(v){
          if(!v){
            return;
          }
          return $filter('date')(v, attrs.fgDateFormatter);
        });
      }
    };
  });
