'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzCity
 * @description
 * # bzCity
 */
angular.module('projApp')
  .directive('bzIssueTickets', function (localStorageService, bzDicts) {
    return {
      require: '?ngModel',
      restrict: 'A',
      templateUrl: 'templates/bzIssueTickets.html',
      scope: {
        record: '=?',//订单
        sups: '=?'//供应商
      },
      link: function(scope, element, attrs ,ngModel){

        var UNISSUE = scope.UNISSUE = '2';//未改签
        var ISSUEING = scope.ISSUEING = '3';//改签中
        var ISSUED = scope.ISSUED = '4';//已改签
        var ISSUE_FAIL = scope.ISSUE_FAIL = '5';//改签失败

        if(!ngModel){
          return;
        }

        bzDicts.query('PAY_TYPE')//支付方式
          .then(function(data){
            scope.payTypes = data;
          });

        scope.$watch('sups', function(sups){
          if(!sups){
            return;
          }
          scope.dictSups = _.map(sups, function(v){
            return {
              name: v.supplierName,
              code: v.supplierId
            }
          });
        });

        var curIssueState = localStorageService.get('CUR_ISSUE_STATE');
        scope.curState = curIssueState? curIssueState : UNISSUE;

        ngModel.$formatters.push(function(value){
          if(!Array.isArray(value)){
            return;
          }
          scope.issues = value;
          scope.issues.supId = scope.dictSups[0].code;
        });
      }
    };
  });
