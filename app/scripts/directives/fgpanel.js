'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgPanel
 * @description
 * # copy from mcasimir/mobile-angular-ui
 */
angular.module('projApp')
  .directive('fgPanel', function () {
    return {
      restrict: 'EA',
      replace: true,
      scope: false,
      transclude: true,
      link: function(scope, elem) {
        elem.removeAttr('title');
      },
      template: function(elems, attrs) {
        var heading = '';
        if (attrs.title) {
          heading = '<div class="panel-heading"><h3 class="panel-title">' + attrs.title + '</h3></div>';
        }
        return '<div class="panel">' + heading + '<div class="panel-body"><div ng-transclude></div></div></div>';
      }
    };
  });
