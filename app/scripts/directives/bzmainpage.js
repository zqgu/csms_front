'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzMainPage
 * @description
 * # bzMainPage
 */
angular.module('projApp')
  .directive('bzMainPage', function ($rootScope) {
    return {
      templateUrl: function (element, attrs){
        return attrs.bzMainPage;
      },
      restrict: 'EAC',
      scope: {},
      link: function postLink(scope) {
        scope.case = $rootScope.case;
        scope.myInterval = 5000;
        var slides = scope.slides = [];

        var addSlide = function() {
          slides.push({
            image: 'data:image/gif;base64,R0lGODlhAQABAIAAAFVVVQAAACH5BAAAAAAALAAAAAABAAEAAAICRAEAOw==',
            text: ['More','Extra','Lots of','Surplus'][slides.length % 4] + ' ' +
              ['Cats', 'Kittys', 'Felines', 'Cutes'][slides.length % 4]
          });
        };
        for (var i=0; i<4; i++) {
          addSlide();
        }

        scope.login = function(){
          $rootScope.selection = 'login';
        };
      }
    };
  });
