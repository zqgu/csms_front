'use strict';

/**
 * @ngdoc directive
 * @name wsdcApp.directive:fgAppNavbar
 * @description
 * # fgAppNavbar
 */
angular.module('projApp')
  .constant('bzMenus', [{
    menuName: '国内机票',
    path: 'flights'
  },{
    menuName: '电话预定',
    path: 'orderWizards/step1'
  },{
    menuName: '出票订单',
    path: 'tickets/issues'
  },{
    menuName: '退票订单',
    path: 'tickets/returns'
  }])
  // 系统导航栏 》菜单
  .directive('fgAppNavbarMenu', function ($http, $window, localStorageService, bzMenus) {
    return {
      templateUrl: 'templates/fgAppNavbarMenu.html',
      restrict: 'EA',
      replace: true,
      scope: {},
      link: function postLink(scope) {
        var user = localStorageService.get('user');
        scope.menus = bzMenus;
      }
    };
  })
  // 系统导航栏
  .directive('fgAppNavbar', function (localStorageService) {
    return {
      templateUrl: 'templates/fgAppNavbar.html',
      restrict: 'EA',
      replace: true,
      scope: {},
      link: function postLink(scope) {

        scope.title = '客服系统';

        scope.isAuthed = function (){
          return localStorageService.get('token');
        };
      }
    };
  });
