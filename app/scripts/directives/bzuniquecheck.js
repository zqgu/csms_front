'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzUniqueCheck
 * @description
 * # bzUniqueCheck
 */
angular.module('projApp')
  .directive('bzUniqueCheck', function ($http) {
    return {
      restrict: 'AC',
      require: '?ngModel',
      link: function postLink(scope, element, attrs, ngModel) {
        var checkType = attrs.bzUniqueCheck;

        if(!ngModel || !checkType){
          return;
        }

        var onBlur = function () {
          scope.$apply(check);
        };

        element.on('blur', onBlur);

        var check = function () {
          var params = {};
          params[checkType] = ngModel.$viewValue;

          $http.post('api/users/checkUnique', params, {
            timeout: 500
          }).success(function (data){
            var result = data.results;
            if(result[checkType]){
              ngModel.$setValidity('uniqueErr', false);
            }else{
              ngModel.$setValidity('uniqueErr', true);
            }
          }).error(function (){
            ngModel.$setValidity('uniqueErr', true);
          });
        };

      }
    };
  });
