'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:fgSiderNav
 * @description
 * # fgSiderNav
 */
angular.module('projApp')
  .directive('fgSiderNav', function ($location, $rootScope) {
    return {
      templateUrl: 'templates/fgSiderNav.html',
      restrict: 'AC',
      scope: {},
      link: function postLink(scope, element, attrs) {

        $rootScope.$watch('path', function (path) {
          scope.path = path;

          if(path === '/flights' || path === '/welcome' || path === '/flts'){
            $(element).hide().siblings().removeClass('col-sm-10').addClass('col-sm-12');
          }else{
            $(element).show().siblings().removeClass('col-sm-12').addClass('col-sm-10');
          }
        });

      }
    };
  });
