'use strict';

/**
 * @ngdoc directive
 * @name wsdcApp.directive:fgAppNavbar
 * @description
 * # fgAppNavbar
 */

angular.module('projApp')
  .directive('fgTagsinput', function () {
    return {
      restrict: 'A',
      require: '?ngModel',
      scope: {},
      link: function postLink(scope, element, attrs, ngModel) {

        if(!ngModel){
          return ;
        }

        var inputOption = {};

        if(!attrs.itemValue){
          console.debug('当传递对象时，必须设置itemValue属性');
          return;
        }
        inputOption.itemValue = attrs.itemValue;
        if(attrs.itemText){
          inputOption.itemText = attrs.itemText;
        }
        if(attrs.maxTags){
          inputOption.maxTags = attrs.maxTags;
        }
        element.tagsinput(inputOption);//初始化组件

        $('.bootstrap-tagsinput').addClass('form-control').css({'margin-bottom': 0});//调整样式

        scope.$watch(//ngModel无法深度监听对象数组
          function() {
            return ngModel.$modelValue;
          }, function(modelValue) {
            if(!modelValue){
              return;
            }

            var copyModelValue = angular.copy(modelValue);

            element.tagsinput('removeAll');//removeAll()后，modelValue为空

            if(angular.isArray(copyModelValue)){
              _.each(copyModelValue, function(v){
                if(!v.id|| !v.name){//不添加空对象
                  return ;
                }
                element.tagsinput('add', v);
              })
            }else if(angular.isObject(copyModelValue)){
              if(!copyModelValue.id|| !copyModelValue.name){//不添加空对象
                return ;
              }
              element.tagsinput('add', copyModelValue);
            }

          }, true);

        ngModel.$parsers.push(function(value){
          if(angular.isArray(value)){//如果value是字符串，返回inputs的数组值，否则返回value
            return value;
          }else{
            return element.tagsinput('items');
          }
        });

      }
    };
  });
