'use strict';

/**
 * @ngdoc directive
 * @name projApp.directive:bzAirTicketRequest
 * @description
 * # bzAirTicketRequest
 */
angular.module('projApp')
  .filter('paginator', function(){
    return function(array, page, size){
        if(!array) {return;}
        var start = size * (page - 1);
        return array.slice(start, start + size);
    };
  })
  .filter('dictFilter', function(){//字典过滤
    return function(input, dicts){
      if('undefined' == typeof input || input == null){
        return ;
      }
      return _.find(dicts, {code: input}).name;
    }
  })
  .filter('customCurrency', ['$filter', function ($filter) {
    return function(amount, currencySymbol){
      var currency = $filter('currency');

      if(amount < 0){
        return currency(amount, currencySymbol).replace('(', '').replace(')', '').replace(/\s/, ' -' );
      }

      return currency(amount, currencySymbol);
    };

  }])
  .filter('showNamesFilter', function () {
    return function (input) {
      if(!input){
        return;
      }
      return _.pluck(input, 'name').join(',');
    };
  });
