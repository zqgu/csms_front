'use strict';

/**
 * @ngdoc service
 * @name projApp.fgCityService
 * @description
 * # fgCityService
 * Service in the projApp.
 */
angular.module('projApp')
  .filter('fgFilterCity', function (fgCityService){
    return function (input){
      if(!input){
        return;
      }
      var name = fgCityService.getCityByCode(input).CityName;
      return name;
    };
  })
  .service('fgCityService', function fgCityService(fgCityData) {

    //根据全拼查询城市
    this.fullLetters = function (value){
      if(!value){
        return [];
      }

      var results = _.filter(fgCityData, function (city){
        if(city.QuanPin.indexOf(value) !== -1){
          return true;
        }
      });

      return results;
    };

    //根据三字码查询城市
    this.cityCode = function (value){

      if(!value){
        return [];
      }

      var results = _.filter(fgCityData, function (city){
        if(city.CityCode.indexOf(value.toUpperCase()) !== -1){
          return true;
        }
      });

      return results;
    };
    
    //根据首字母查询城市
    this.firstLetter = function (value){
      if(!value){
        return [];
      }

      var results = _.filter(fgCityData, function (city){
        if(city.ShouZiMu.indexOf(value.toUpperCase()) !== -1){
          return true;
        }
      });

      return results;
    };

    //根据中文名查询城市
    this.cityName = function (value){
      if(!value){
        return [];
      }

      var results = _.filter(fgCityData, function (city){
        if(city.CityName.indexOf(value.toUpperCase()) !== -1){
          return true;
        }
      });

      return results;
    };

    //根据机场查询城市
    this.airPort = function (value){
      if(!value){
        return [];
      }

      var results = _.filter(fgCityData, function (city){
        if(_.contains(city.Airport.split(','),value)){
          return true;
        }
      });

      return results;
    };

    //根据输入值获得匹配城市数据
    this.matchedCity = function(value){

      if(!value){
        return [];
      }

      //var chnRe  = new RegExp('^[\\u4e00-\\u9fa5]$');

      //var enRe = new RegExp('^[A-Za-z]+$');

      //返回值初始化
      var output = [];

      //如果长度大于3
      if(value.length > 3){
        output = _.filter(fgCityData, function (city){
          if((city.CityName.indexOf(value) !== -1 ) ||
              (city.CityCode.indexOf(value.toUpperCase()) !== -1 ) ||
              (city.ShouZiMu.indexOf(value.toUpperCase()) !== -1 ) ||
              (_.contains(city.Airport.split(','),value.toUpperCase())) ||
              (city.QuanPin.indexOf(value) !== -1)

          ){
            return true;
          }
        });
      }else{
         output = _.filter(fgCityData, function (city){
          if((city.CityName.indexOf(value) !== -1 ) ||
              (city.CityCode.indexOf(value.toUpperCase()) !== -1 ) ||
              (city.ShouZiMu.indexOf(value.toUpperCase()) !== -1 ) ||
              (_.contains(city.Airport.split(','),value.toUpperCase()))
          ){
            return true;
          }
        });   
      }
      
      return output;
    };

    //三字码转换为城市名称
    this.getCityByCode = function(code){
      var s = _.find(fgCityData,function(city){
        return city.CityCode === code;
      });
      if(s){return s;}
    };

    //根据首字母查询城市
    this.getCitiesBySzm = function (keys){
      if(!keys){
        return;
      }

      var values = keys.toUpperCase().split('');

      return _.filter(fgCityData, function (city){
        var szm = city.ShouZiMu.split('')[0];
        if(values.indexOf(szm) !== -1 && city.Airport) {
          return true;
        }
      });
    };

    //获取热门城市
    this.getHotCities = function () {
      return _.filter(fgCityData, function (city) {
        return city.hot;
      });
    };

  });
