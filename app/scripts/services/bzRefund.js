'use strict';

/**
 * @ngdoc service
 * @name projApp.bzDicts
 * @description
 * # bzDicts
 * Service in the projApp.
 */
angular.module('projApp')
  .service('bzRefund', function (localStorageService) {
    this.getState = function() {
      return localStorageService.get('refundState');
    };

    this.setState = function(state) {
      localStorageService.set('refundState', state);
    };
  });
