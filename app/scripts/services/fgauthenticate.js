'use strict';

/**
 * @ngdoc service
 * @name projApp.fgAuthenticate
 * @description
 * # fgAuthenticate
 * Service in the projApp.
 */
angular.module('projApp')
  //将跳转前的 path 作为 redirect 参数，传递至登录视图
  .factory('fgLoginRedirect', function ($rootScope, $location){
    return {
      go: function (){
        //将跳转前的 path 作为 redirect 参数，传递至登录视图
        var curPath = $location.path().replace(/\//, '');
        if(curPath !== '' && curPath !== 'login'){
          $location.search('redirect', curPath);
        }
        $location.path('/login');
      }
    };
  })
  //用于跳转至登录页
  .directive('fgLogin', function (fgLoginRedirect){
    return {
      restrict: 'AC',
      link: function (scope, element){
        element.on('click', fgLoginRedirect.go);
      }
    };
  })
  //用于用户登出,登出后页面跳转至主页面
  .directive('fgLogout', function (fgAuthenticate, $location){
    return {
      restrict: 'AC',
      link: function (scope, element, attrs){
        var onClick = function () {
          fgAuthenticate.logout();
          //跳转至指定 route，默认为
          $location.path(attrs.fgLogout);
        };

        element.on('click', onClick);
      }
    };
  })
  .factory('fgAuthHttpInterceptor', function ($window, localStorageService, $rootScope, fgLoginRedirect) {
    return {
      response: function (response) {
        if(response.data && response.data.exceptionMsg === '401'){
          $window.alert('登录超时，请重新登录');
          $rootScope.logined = false;
          localStorageService.clearAll();
          fgLoginRedirect.go();
        }
        return response || $q.when(response);
      }
    };
  })
  .provider('fgAuthenticate', function () {
    var LOGIN_URL = 'api/employees/login';
    var LOGOUT_URL = 'api/employees/logout';

    this.$get = function ($http, $location, $window, localStorageService, $rootScope, fgLoginRedirect) {
      return {
        login: function (user){

          return $http.post(LOGIN_URL, user).success(function (data){
            if(!data.success){
              return;
            }
            //登录成功后，查看当前url是否包含redirect参数，如果有，则跳转至redirect参数指定的视图
            var searchObject = $location.search();
            var redirect = searchObject.redirect || '/welcome';

            $location.path(redirect);

            $rootScope.logined = data.token;

            localStorageService.set('token', data.token);
            localStorageService.set('user', data.results);

          }).error(function (){
            //登录失败删除 sessionStorage 中的 token 值
            localStorageService.remove('token');
          });
        },
        logout: function (){
          $rootScope.logined = false;
          //清除 token
          localStorageService.clearAll();

          $http.post(LOGOUT_URL).success(function (){
            //清除 token
            localStorageService.remove('token');
          }).error(function (){
            //清除 token
            localStorageService.remove('token');
          });
        }
      };
    };
  });
