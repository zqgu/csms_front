'use strict';

/**
 * @ngdoc service
 * @name projApp.bzDicts
 * @description
 * # bzDicts
 * Service in the projApp.
 */
angular.module('projApp')
  .service('fgCache', function (localStorageService){
    this.bind = function ($scope, property, key) {
      if(!localStorageService.get(key)){
        localStorageService.set(key, {});
      }
      
      var unbind = localStorageService.bind($scope, key);
      $scope[property] = $scope[key];
      return function (){
        unbind();
        localStorageService.remove(key);
      };
    };
  })
  .service('bzDicts', function bzDicts($http, $q, localStorageService) {
    //数据服务器请求中
    var processing = false;

    var url = 'api/dicts';

    //根据字典类型查询
    this.query = function (value) {
      
      var deferred = $q.defer();

      //从缓存中获取
      var key = 'DICT_' + value;
      var results = localStorageService.get(key);
      
      //当缓存中有数据时
      if (results && angular.isObject(results) && results.length > 0) {
        deferred.resolve(results);
      }
      //缓存中没有数据且数据不在请求中时
      else if (!processing) {
        //标记请求置为 true ，标示正在进行中，直到请求完成
        processing = true;

        $http.get(url, {params: {dictTypeCode:value}}).success(function (data) {
          localStorageService.set(key, data.results);
          deferred.resolve(data.results);
        }).error(function (){
          deferred.reject();
        })['finally'](function (){
          //请求完成不论是否成功，将请求状态置为 false
          processing = false;
          console.log('finally');
        });
      }

      return deferred.promise;
    };
  });
