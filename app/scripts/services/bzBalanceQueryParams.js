'use strict';

/**
 * @ngdoc service
 * @name projApp.bzDicts
 * @description
 * # bzDicts
 * Service in the projApp.
 */
angular.module('projApp')
  .service('bzBalanceQueryParams', function (localStorageService) {
    this.set = function(v){
      localStorageService.set('bzBalanceQueryParams', v);
    };

    this.get = function(){
      return localStorageService.get('bzBalanceQueryParams');
    };
  });
