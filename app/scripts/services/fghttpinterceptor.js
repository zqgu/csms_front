'use strict';

/**
 * @ngdoc service
 * @name projApp.fgHttpInterceptor
 * @description
 * # fgHttpInterceptor
 * Service in the projApp.
 */
angular.module('projApp')
  .value('RE_API_URL', /^api/)
  .constant('httpRequests', [])//存储 http 请求
  .factory('fgHttpInterceptor', function ($q, fgMessenger, RE_API_URL, $window, fgLoginRedirect, httpRequests, $rootScope, localStorageService) {

    var method = ['POST', 'PUT', 'DELETE'];

    return {
      request: function (config) {
        config.headers = config.headers || {};

        //请求 header 中 notCancel 值为 true 的请求不会被取消
        if(!config.headers.notCancel){
          var deferred = $q.defer();
          config.timeout = deferred.promise;

          httpRequests.push({
            defer: deferred,
            url: config.url
          });
        }

        if (localStorageService.get('token')) {
          config.headers.AuthorizationToken = localStorageService.get('token');
        }
        return config;
      },

      //全局处理服务端请求错误
      responseError: function(rejection) {
        if(rejection.status === 0){
          return $q.reject(rejection);
        }
        
        var msg = '服务端 (' + rejection.status + ') 错误!\n' +
          rejection.config.method + ': ' + rejection.config.url + '\n' +
          JSON.stringify(rejection.config.data, null, 2);

        fgMessenger.post({
          id: 'fgHttpInterceptor-error',
          message: msg,
          type: 'error',
          showCloseButton: true
        });

        return $q.reject(rejection);
      },

      response: function (response){
        //如果请求地址是 api/*
        if(response && response.config && RE_API_URL.test(response.config.url)){
          var success = response.data.success;

          //如果操作成功
          if(success){
            var showMsg = _.indexOf(method, response.config.method);
            if(showMsg !== -1){
              fgMessenger.post({
                id: 'fgHttpInterceptor-success',
                message: response.data.msg,
                type: 'success',
                showCloseButton: true
              });
            }
          }else{
            fgMessenger.post({
              id: 'fgHttpInterceptor-error',
              message: response.data.msg + ':' + response.data.customMsg,
              type: 'error',
              showCloseButton: true
            });
          }
        }
        return response || $q.when(response);
      }

    };
  });
