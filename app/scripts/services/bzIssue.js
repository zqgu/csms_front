'use strict';

/**
 * @ngdoc service
 * @name projApp.bzDicts
 * @description
 * # bzDicts
 * Service in the projApp.
 */
angular.module('projApp')
  .service('bzIssue', function (localStorageService) {
    this.getState = function() {
      return localStorageService.get('IssueState');
    };

    this.setState = function(state) {
      localStorageService.set('IssueState', state);
    };
  });
