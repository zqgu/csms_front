'use strict';

/**
 * @ngdoc service
 * @name projApp.fgMessenger
 * @description
 * # fgMessenger
 * Service in the projApp.
 */
angular.module('projApp')
  .factory('fgValidatorRoute', function ($q, $route, localStorageService) {
    return {
      validate: function(){
        var
          defer = $q.defer(),
          originalPath = $route.current.originalPath,
          path,//例：role/:id
          i,
          menus = localStorageService.get('menus');//缓存的菜单

        if(originalPath && originalPath !== '/'){
          path= originalPath.substring(1);//截取/后面的部分
          i = _.findIndex(menus, function(v){
            return v.path && (v.path === path);
          });
        }
        if(originalPath !== '/' && i !== -1){
          defer.resolve();
        }else{
          defer.reject();
        }
        return defer.promise ;
      }
    }
  });
