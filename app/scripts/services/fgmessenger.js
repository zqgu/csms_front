'use strict';

/**
 * @ngdoc service
 * @name projApp.fgMessenger
 * @description
 * # fgMessenger
 * Service in the projApp.
 */
angular.module('projApp')
  .provider('fgMessenger', function () {

    // Private variables
    // var salutation = 'Hello';

    var instance = window.Messenger({
      theme: 'air',
      extraClasses: 'messenger-fixed messenger-on-right messenger-on-top'
    });

    // this.setTheme = function (){
      
    // };

    // Method for instantiating
    this.$get = function () {

      /*instance.confirm = function (options) {
        //判断参数是否提供 actions 配置, 且 options.actions 是 object
        if ( _.has(options, 'actions') && _.isObject(options.actions) ){
          var actions = options.actions, newActions = {};

          _.each(actions, function (value, key){
            var label = _.has(value, 'label') ? value.label : key,
                action = (_.has(value, 'action') && _.isFunction(value.action)) ? value.action : angular.noop;

            newActions[key] = {
              label: label,
              action: function (){
                action.apply(this, arguments);
                this.hide();
                $rootScope.$apply();
              }
            };
          });

          options.actions = newActions;
        }
        instance.post.apply(instance, [options]);
      };*/

      return instance;
    };
  });
