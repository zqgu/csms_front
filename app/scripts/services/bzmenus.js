'use strict';

/**
 * @ngdoc service
 * @name projApp.bzDicts
 * @description
 * # bzDicts
 * Service in the projApp.
 */
angular.module('projApp')
  .service('bzmenus', function bzMenus($http, $q, localStorageService) {
    //数据服务器请求中
    var processing = false;

    var url = 'api/menus';

    //根据字典类型查询
    this.query = function (userId) {

      var deferred = $q.defer();

      //从缓存中获取
      var results = localStorageService.get('menus' + userId);

      //当缓存中有数据时
      if (results && angular.isObject(results) && results.length > 0) {
        deferred.resolve(results);
      }
      //缓存中没有数据且数据不在请求中时
      else if (!processing) {
        //标记请求置为 true ，标示正在进行中，直到请求完成
        processing = true;

        //切换 route，不中断改请求
        var header = {
          headers: {notCancel: true, params: {}}
        };

        if(userId){
          header.headers.params.userId = userId;
        }

        $http.get(url, header).success(function (data) {
          localStorageService.set('menus' + userId, data.results);
          deferred.resolve(data.results);
        }).error(function (){
          deferred.reject();
        })['finally'](function (){
          //请求完成不论是否成功，将请求状态置为 false
          processing = false;
        });
      }

      return deferred.promise;
    };
  });
