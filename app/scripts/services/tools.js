'use strict';

/**
 * @ngdoc service
 * @name projApp.tools
 * @description
 * # bzDicts
 * Service in the projApp.
 */
angular.module('projApp')
  .service('tools', function (){
    this.convert2Tree = function (nodes, id, parentId) {

      var map = {}, node, roots = [];

      _.each(nodes, function(v, i){//将（menuId, id）键值对存储于map,避免通过遍历数组查找父节点
        map[v[id]] = i;
      });

      _.each(nodes, function(v) {
        node = v;

        if (!(node.children instanceof Array)) {
          v.children = [];
        }

        if (node[parentId] !== 0) {
          var parent = nodes[map[node[parentId]]];

          if (!(parent.children instanceof Array)) {
            parent.children = [];
          }

          nodes[map[node[parentId]]].children.push(node);
        } else {
          roots.push(node);
        }
      });

      return roots;
    };
  });
