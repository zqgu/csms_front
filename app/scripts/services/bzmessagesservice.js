'use strict';

/**
 * @ngdoc service
 * @name projApp.bzMessagesService
 * @description
 * # bzMessagesService
 * Service in the projApp.
 */
angular.module('projApp')
  .service('bzMessagesService', function bzMessagesService($http, $rootScope) {
    var url = 'api/msgAttentions';

    //标记状态为已读
    this.read = function (id) {
      var params = {
        state: {code: '1'}
      };

      //return $http.put(url + '/' + id, params);
    };

    //标记状态为未读
    this.unread = function (id) {
      var params = {
        state: {code: '0'}
      };

      //return $http.put(url + '/' + id, params);
    };

    this.get = function (id) {
      //return $http.get(url + '/' + id).su;
    };

    this.getUnreads = function () {
      /*return $http.get(url, {params: {'state.code': 0}, headers: {notCancel: true}}).success(function (data){
        if(data.success) {
          //广播事件
          $rootScope.$broadcast('bzMessagesService:getUnreads', data.results.length);
          //contorller 里使用 scope.$on('bzMessagesService:getUnreads', function (){//....}) 来订阅
        }
      });*/
    };
  });
