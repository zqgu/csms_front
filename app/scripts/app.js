'use strict';

/**
 * @ngdoc overview
 * @name projApp
 * @description
 * # projApp
 *
 * Main module of the application.
 */
angular
  .module('projApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'templates-templates',
    'ui.bootstrap',
    'angular-loading-bar',
    'cgBusy',
    'LocalStorageModule',
    'fgpopover',
    'flightbook',
    'hotelbook'
  ])
  .config(function (localStorageServiceProvider) {
    localStorageServiceProvider
      .setPrefix('projApp')
      .setStorageType('sessionStorage')
      .setNotify(true, true);
  })
  .config(function ($httpProvider){
    $httpProvider.interceptors.push('fgHttpInterceptor');
    $httpProvider.interceptors.push('fgBookFlightsHttpInterceptor');
    $httpProvider.interceptors.push('fgAuthHttpInterceptor');
    $httpProvider.interceptors.push('fgBookHotelsHttpInterceptor');
  })
  .config(function ($routeProvider) {
    var authorize = {
      authorize: ['fgValidatorRoute', function (fgValidatorRoute) {
        fgValidatorRoute.validate();
      }]
    };

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/flights', {
        templateUrl: 'views/flights.html',
        controller: 'FlightsCtrl'
      })
      .when('/bookFlight', {
        templateUrl: 'views/bookFlight.html',
        controller: 'BookFlightCtrl'
      })
      .when('/welcome', {
        templateUrl: 'views/welcome.html',
        controller: 'WelcomeCtrl'
      })
      .when('/orderWizards/step1', {
        templateUrl: 'views/orderwizardsstep1.html',
        controller: 'Orderwizardsstep1Ctrl'
      })
      .when('/hotelorderWizards/step1', {
        templateUrl: 'views/hotelorderwizardsstep1.html',
        controller: 'Hotelorderwizardsstep1Ctrl'
      })
      .when('/tickets/issues', {
        templateUrl: 'views/ticketsIssues.html',
        controller: 'TicketsIssuesCtrl'
      })
      .when('/tickets/issues/:id', {
        templateUrl: 'views/ticketsIssue.html',
        controller: 'TicketsIssueCtrl'
      })
      /*.when('/tickets/returns', {
        templateUrl: 'views/ticketsreturned.html',
        controller: 'TicketsreturnedCtrl'
      })
      .when('/tickets/returns/:id', {
        templateUrl: 'views/ticketsreturnedDetail.html',
        controller: 'TicketsreturnedDetailCtrl'
      })
      .when('/tickets/changes', {
        templateUrl: 'views/ticketsChanges.html',
        controller: 'TicketsChangesCtrl'
      })
      .when('/tickets/changes/:id', {
        templateUrl: 'views/ticketsChangesDetail.html',
        controller: 'TicketsChangesDetailCtrl'
      })*/
      .when('/orderWizards/step2/:mobile', {
        templateUrl: 'views/orderwizardsstep2.html',
        controller: 'Orderwizardsstep2Ctrl'
      })
      .when('/purBalance', {
        templateUrl: 'views/purBalance.html',
        controller: 'PurBalanceCtrl'
      })
      .when('/supBalance', {
        templateUrl: 'views/supBalance.html',
        controller: 'SupBalanceCtrl'
      })
      .when('/hotels/orders/create', {//酒店订单
        templateUrl: 'views/hotelOrder.html',
        controller: 'HotelOrderCtrl'
      })
      .when('/my/hotels/orders', {//酒店订单列表
        templateUrl: 'views/myHotelOrders.html',
        controller: 'MyHotelOrdersCtrl'
      })
      .when('/my/hotels/orders/:id', {//酒店订单详细
        templateUrl: 'views/myHotelOrder.html',
        controller: 'MyHotelOrderCtrl'
      })
      /*.when('/inter/tickets/issues', {//国际出票订单列表
        templateUrl: 'views/interTicketsIssues.html',
        controller: 'InterTicketsIssuesCtrl'
      })
      .when('/inter/tickets/issues/:id', {//国际出票订单详细
        templateUrl: 'views/interTicketsIssue.html',
        controller: 'InterTicketsIssueCtrl'
      })*/
      .when('/inter/tickets/changes', {//国际改签订单列表
        templateUrl: 'views/interTicketsChanges.html',
        controller: 'InterTicketsChangesCtrl'
      })
      .when('/inter/tickets/changes/:id', {//国际改签订单详细
        templateUrl: 'views/interTicketsChange.html',
        controller: 'InterTicketsChangeCtrl'
      })
      .when('/inter/tickets/refunds', {//国际退票订单列表
        templateUrl: 'views/interTicketsRefunds.html',
        controller: 'InterTicketsRefundsCtrl'
      })
      .when('/inter/tickets/refunds/:id', {//国际退票订单详细
        templateUrl: 'views/interTicketsRefund.html',
        controller: 'InterTicketsRefundCtrl'
      })
      .when('/tickets/issues', {//出票订单列表
        templateUrl: 'views/ticketsIssues.html',
        controller: 'TicketsIssuesCtrl'
      })
      .when('/tickets/issues/:id', {//出票订单详细
        templateUrl: 'views/ticketsIssue.html',
        controller: 'TicketsIssueCtrl'
      })
      .when('/tickets/changes', {//改签订单列表
        templateUrl: 'views/ticketsChanges.html',
        controller: 'TicketsChangesCtrl'
      })
      .when('/tickets/changes/:id', {//改签订单详细
        templateUrl: 'views/ticketsChange.html',
        controller: 'TicketsChangeCtrl'
      })
      .when('/tickets/refunds', {//退票订单列表
        templateUrl: 'views/ticketsRefunds.html',
        controller: 'TicketsRefundsCtrl'
      })
      .when('/tickets/refunds/:id', {//退票订单详细
        templateUrl: 'views/ticketsRefund.html',
        controller: 'TicketsRefundCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function ($rootScope, $location, fgMessenger, httpRequests, $window, localStorageService) {

    $rootScope.logined = localStorageService.get('token');

    $rootScope.items = ['login', 'passwordReset'];
    $rootScope.selection = $rootScope.items[0];

    $rootScope.$on('$viewContentLoaded', function () {
      //切换视图时关闭所有 Messagebox
      // fgMessenger.hideAll();

      //去除非 login route 时 url 中的 redirect 参数
      var path = $rootScope.path = $location.path();
      if(path !== '/login'){
        $location.search('redirect',null);
      }

      $('#abc').removeClass().addClass('path-' + path.replace(/\//g, ''));

      //切换 route 成功时，cancel 之前的 request，
      _.each(httpRequests, function (value) {
        value.defer.resolve();
        // console.log(value.url);
      });

      httpRequests.length = 0;
    });

    $('body').on('click', function (e) {//当点击popover以外部分，隐藏popover
      if (!$(e.target).next().hasClass('popover') &&
        $(e.target).parents('.popover.in').length === 0) {
        $('.popover').popover('hide');
      }
      if(e.target.className === 'jstree-icon jstree-ocl'){
        e.stopPropagation();
      }
    });
  });
