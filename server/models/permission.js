"use strict";

module.exports = function(sequelize, DataTypes) {
  var Permission = sequelize.define('Permission', {
    privId: {
      type: DataTypes.BIGINT(20),
      primaryKey: true,
      field: 'priv_id'
    },
    privCode: {
      type: DataTypes.STRING(50),
      field: 'priv_code'
    },
    privName: {
      type: DataTypes.STRING(100),
      field: 'priv_name'
    },
    detail: DataTypes.STRING(200),
    sysType: {
      type: DataTypes.STRING(20),
      field: 'sys_type'
    },
    remark: DataTypes.STRING(500),
    privType: {
      type: DataTypes.STRING(20),
      field: 'priv_type'
    },
  }, {
    tableName: 't_base_privilege',
    timestamps: false
  });

  return Permission;
};
