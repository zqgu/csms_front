"use strict";

module.exports = function(sequelize, DataTypes) {
  var Country = sequelize.define("Country", {
    countryId: {
      type: DataTypes.BIGINT(20),
      primaryKey: true,
      field: 'country_id'
    },
    countryCode: {
      type: DataTypes.STRING(100),
      field: 'country_code'
    },
    countryName: {
      type: DataTypes.STRING(100),
      field: 'country_name'
    },
    sort: DataTypes.INTEGER(11)
  }, {
    tableName: 't_base_country',
    timestamps: false
  });

  return Country;
};
