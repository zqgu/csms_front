"use strict";

var soap = require('soap');
var xml2js = require('xml2js');
var _ = require('underscore');
var when = require('when');

var url = 'http://ibe.51ttu.com/IBE.svc?wsdl';

//配置解析器
var parser = new xml2js.Parser({
  explicitArray: false,
  mergeAttrs: true
});

var findFD = function (param) {
  var deferred = when.defer();

  soap.createClient(url, function(err, client) {

    if(err) {
      deferred.reject(err);
    };

    client.FD(param, function(err, result) {
      if(err) {
        deferred.reject(err);
      }

      parser.parseString(result.FDResult, function (err, data) {
        try{
          var items = data['LA.FD'].FdList.Item;
          deferred.resolve([items, param]);
        }catch(e){
          deferred.reject(e);
        }
      });
    });

  });
  return deferred.promise;
};

//匹配价格
var matchPrice = function (flights, prices) {
  for (var i = 0; i < flights.length; i++) {
    var flight = flights[i];

    for (var j = 0; j < flight.CabinList.Cabin.length; j++) {
      var cabin = flight.CabinList.Cabin[j];

      for (var k = 0; k < prices.length; k++) {
        var price = prices[k];

        if(price.Ezm === flight.Ezm && price.Cabin === cabin.CabinName){
          _.extend(cabin, price);
        }

      };
    };
  };
};

var findFlight = function (param, prices) {

  var deferred = when.defer();

  soap.createClient(url, function(err, client) {

    if(err) {
      throw err;
      deferred.resolve([]);
    };
    
    client.AVH(param, function(err, result) {
      if(err) {
        throw err;
        deferred.resolve([]);
      }

      if(result.AVHResult === 'ERROR'){
        deferred.resolve([]);
        return;
      }

      parser.parseString(result.AVHResult, function (err, data) {
        if(err){
          throw err;
          deferred.resolve([]);
        }
        try{
          var flights = data['LA.AV'].FlightList.Flight;
          matchPrice(flights, prices);
          deferred.resolve(flights);
        }catch(e){
          deferred.resolve([]);
        }
      });
    });

  });

  return deferred.promise;
};

var mid = function (value) {
  var deferreds = [];

  var items = value[0],
      param = value[1];

  var results = [];

  for (var i = 0; i < items.length; i++) {
    var item = items[i];
    if(results.indexOf(item.Ezm) === -1){
      results.push(item.Ezm);
    }
  };

  var deferreds = [];

  for (var i = 0; i < results.length; i++) {
    var airCompany = results[i];
    
    var data = {
      startCity: param.startCity,
      endCity: param.endCity,
      airCompany: airCompany,
      flyDate: param.flyDate,
      CPID: 1,
      SecurityCode: '6OB7VO+kmAA='
    };

    var cabinPrices = _.filter(items, function (item){
      if(item.Ezm === airCompany){
        return true;
      }
    });

    deferreds.push(findFlight(data, cabinPrices));
  };

  return deferreds;
};

//查询条件
// var param = {
//   startCity: 'SHA',
//   endCity: 'PEK',
//   airCompany: 'MU',
//   flyDate: '2014-12-30',
//   CPID: 1,
//   SecurityCode: '6OB7VO+kmAA='
// };


// when.all(findFD(param).then(mid)).then(function (results){
//   var data = _.flatten(results);
//   console.log(data.length);
//   console.log(JSON.stringify(data[0],null,2));
// });


var query = function (param){
  console.log(JSON.stringify(param));
  
  var deferred = when.defer();

  when.all(findFD(param).then(mid)).then(function (results){
    var data = _.flatten(results);
    // console.log(data.length);
    // console.log(JSON.stringify(data[0],null,2));
    deferred.resolve(data);
  });

  return deferred.promise;
};

exports.query = query;
