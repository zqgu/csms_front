"use strict";

var express = require('express');
var router = express.Router();

var data = {
  success: true,
  results: [{
    menuId: 1,
    menuName: '主菜单'
  }]
};

/* GET menus listing. */
router.get('/', function(req, res) {
  res.send(data);
});

module.exports = router;