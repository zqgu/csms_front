var express = require('express');
var router = express.Router();
var soap = require('soap');
var xml2js = require('xml2js');
var shopping = require('./shopping');
var _ = require('underscore');

var url = 'http://ibe.51ttu.com/IBE.svc?wsdl';

//配置解析器
var parser = new xml2js.Parser({
  explicitArray: false,
  mergeAttrs: true
});

//读取航空公司数据
var airlines = require('../config/airlines.json');

var airlinesCache = {};

_.each(airlines, function (airline){
  airlinesCache[airline.AirLine] = airline;
});

//读取机场数据
var airports = require('../config/airports.json');

var airportsCache = {};

_.each(airports, function (airport){
  airportsCache[airport.AirPort] = airport;
});

//读取飞机机型数据
var planes = require('../config/planes.json');

var planesCache = {};

_.each(planes, function (plane){
  planesCache[plane.CraftType] = plane;
});

//读取城市数据
var cities = require('../config/cities.json');

var citiesCache = {};

_.each(cities, function (city){
  citiesCache[city.CityCode] = city;
});

//读取航空公司舱位对应表数据
var airlineCabins = require('../config/airlineCabins.json');

var getCabinClassNameByAirlineAndCabinName = function (airline, cabinName) {
  var cabins = airlineCabins[airline];
  var result = cabinName;
  if(cabins){
    for(var key in cabins) {
      var collections = cabins[key];
      if(collections.indexOf(cabinName) !== -1){
        result = key;
      }
    }
  }
  return result;
};

var cabinNameDict = {
  'Y': '经济舱',
  'C': '商务舱',
  'F': '头等舱',
  'S': '超级经济舱'
};

//以下代码用于判断时段 0点~6点 1；6点~12点 2；12点~18点 3；18~24点 4
var getPeriod = function (datetime){
  var re = /2\d{3}-\d{1,2}-\d{1,2}\s(\d{1,2}):(\d{1,2}):\d{1,2}/;
  var r = datetime.match(re);
  //输入:'2014-12-12 21:55:00'.match(/2\d{3}-\d{1,2}-\d{1,2}\s(\d{1,2}):(\d{1,2}):\d{1,2}/)
  //输出:["2014-12-12 21:55:00", "21", "55"]
  
  var num;

  if(r && r.length === 3){
    //拼接小时和分钟，用于下一步的计算
    num = r[1] - 0 + r[2] - 0;
  }else{
    //返回 -1，说明时间格式有问题
    return '-1';
  }

  if(num > 0 && num <= 600) {
    return '1';
  }else if (num > 600 && num <= 1200) {
    return '2';
  }else if (num > 1200 && num <= 1800) {
    return '3';
  }else if (num > 1800 && num <= 2400) {
    return '4';
  }else{
    //不可能
    return '5';
  }
};

var periodDict = {
  '1': '00 ~ 06点',
  '2': '06 ~ 12点',
  '3': '12 ~ 18点',
  '4': '18 ~ 24点'
};

//按中文名称排序
var sortFunc = function (a, b) {
  return a.code - b.code;
};

var sortByTime = function (a, b) {
  var t = new Date(a.takeOffTime).getTime() - new Date(b.takeOffTime).getTime();
  return t;
};

//退废票政策
var policies = require('../config/cabinPolicy.json');

var getPolicy = function (airline, cabin) {
  if(policies[airline] && policies[airline][cabin]) {
    return policies[airline][cabin];
  }else{
    return null;
  }
};

//机场距离
var distances = require('../config/distances.json');

//根据出发抵达机场三字码获取机场间距离
var getDistance = function (key) {
  return distances[key];
};

//根据出发抵达机场三字码获取然后附加费
var getFuelCost = function (key) {
  var distance = getDistance(key) - 0;
  if(distance <= 800){
    return 30;
  }else{
    return 60;
  }
};

//转换携程的返回结构为我们定义的结构
var convert = function (flights, startCity, endCity) {
  var results = [];
  var takeOffPeriod = [], arrivePeriod = [], planeType = [], departPort = [] ,arrivePort = [], airlineFilter = [];
  var takeOffPeriodObj = [], arrivePeriodObj = [], departPortObj = [] ,arrivePortObj = [], airlineFilterObj = [];

  for (var i = flights.length - 1; i >= 0; i--) {
    var flight = flights[i];

    var record = {
      cabins: []
    };
    record.stop = flight.stop;
    record.takeOffTime = flight.takeOffTime;
    //计算起飞时段
    record.takeOffPeriod = getPeriod(record.takeOffTime);
    //汇总起飞时段
    var index = takeOffPeriod.indexOf(record.takeOffPeriod);
    if(index === -1){
      takeOffPeriod.push(record.takeOffPeriod);
      takeOffPeriodObj.push({
        code: record.takeOffPeriod,
        name: periodDict[record.takeOffPeriod.toString()],
        items: [i]
      });
    }else{
      takeOffPeriodObj[index].items.push(i);
    }

    record.arriveTime = flight.arriveTime;
    //计算抵达时段
    record.arrivePeriod = getPeriod(record.arriveTime);
    //汇总抵达时段
    var index1 = arrivePeriod.indexOf(record.arrivePeriod)
    if(index1 === -1){
      arrivePeriod.push(record.arrivePeriod);
      arrivePeriodObj.push({
        code: record.arrivePeriod,
        name: periodDict[record.arrivePeriod.toString()],
        items: [i]
      });
    }else{
      arrivePeriodObj[index1].items.push(i);
    }

    record.departCityCode = startCity;
    record.departCityName = citiesCache[startCity] ? citiesCache[startCity].CityName : startCity;

    record.arriveCityCode = endCity;
    record.arriveCityName = citiesCache[endCity] ? citiesCache[endCity].CityName : endCity;

    record.departPortCode = flight.departPortCode;
    record.departPortName = airportsCache[flight.departPortCode].AirPortName;

    //汇总出发机场数据
    if(departPort.indexOf(record.departPortCode) === -1){
      departPort.push(record.departPortCode);
      departPortObj.push({
        code: record.departPortCode,
        name: record.departPortName
      });
    }

    record.arrivePortCode = flight.arrivePortCode;
    record.arrivePortName = airportsCache[flight.arrivePortCode].AirPortName;

    //汇总到达机场数据
    if(arrivePort.indexOf(record.arrivePortCode) === -1){
      arrivePort.push(record.arrivePortCode);
      arrivePortObj.push({
        code: record.arrivePortCode,
        name: record.arrivePortName
      });
    }

    record.departPortBuilding = flight.departPortBuilding;
    record.arrivePortBuilding = flight.arrivePortBuilding;

    //可能存在错误 TypeError: Cannot read property 'ShortName' of undefined
    record.airlineCode = flight.airline;
    record.airlineName = airlinesCache[flight.airline] ? airlinesCache[flight.airline].ShortName : flight.Ezm;

    //汇总航空公司数据
    if(airlineFilter.indexOf(record.airlineCode) === -1){
      airlineFilter.push(record.airlineCode);
      airlineFilterObj.push({
        code: record.airlineCode,
        name: record.airlineName
      });
    }

    record.flightNo = flight.flightNo;
    record.planeType  = flight.planeType;

    //汇总机型数据
    if(planeType.indexOf(record.planeType) === -1){
      planeType.push(record.planeType);
    }

    record.meal = flight.meal;

    record.airportCstFee = '50'
    record.punctualityRate = Math.floor(Math.random() * 11 + 80) + '%';
    //计算然后附加费
    record.fuelCosts = getFuelCost(record.departPortCode + record.arrivePortCode);

    //经济、商务、头等舱基准价格
    var basePrice = {
      y: null, c: null, f: null
    };

    //底价
    record.bottomPrice = Infinity;

    for (var j = flight.cabins.length - 1; j >= 0; j--) {


      var cabin = flight.cabins[j];

      //cabin.Price-0 是因为要将字符串值转换为数字
      if (cabin.Price && (cabin.Price-0) < record.bottomPrice) {
        record.bottomPrice = cabin.Price;
      }

      var cabinClassValue = getCabinClassNameByAirlineAndCabinName(record.airlineCode, cabin.CabinName);

      var ca = {
        cabinClass: cabinClassValue,//舱位等级 Y、C、F
        cabinClassName: cabinNameDict[cabinClassValue],//舱位等级中文名 经济、商务、头等
        cabin: cabin.CabinName,
        price: cabin.Price,
        quantity: cabin.LeftCount,
        //根据航空公司，舱位号获取退改签政策
        policy: getPolicy(record.airlineCode, cabin.CabinName)
      };

      _.extend(ca, cabin);

      //如果航空公司为MU,CZ,MF其中之一，公务舱基准价格设置为 J 舱的价格
      var code = record.airlineCode;
      if (code === 'MU' || code === 'CZ' || code === 'MF') {
        //如果当前舱位为 J，则将商务舱的基准价格设置为当前舱（J）的价格
        if(cabin.CabinName === 'J') {
          basePrice['C'] = cabin.Price;
        }
      }

      if (cabin.CabinName === 'Y' || cabin.CabinName === 'C' || cabin.CabinName === 'F'){
        //首先判断该航班的这个基准舱位价格是否存在
        if(!basePrice[cabin.CabinName]){
          basePrice[cabin.CabinName] = cabin.Price;
        }
      }

      record.cabins.push(ca);

    };

    for(var k=0,size=record.cabins.length; k<size; k++) {
      var cabin = record.cabins[k];
      cabin.discount = cabin.price / basePrice[cabin.cabinClass];
    }

    results.push(record);
  };

  var planes = {};

  _.each(planeType, function (type){
    planes[type] = planesCache[type];
  });

  return {
    filghts: results.sort(sortByTime),
    filter: {
      planeType: planeType, 
      departPort: departPortObj,
      arrivePort: arrivePortObj,
      airline: airlineFilterObj,
      planes: planes,
      arrivePeriod: arrivePeriodObj.sort(sortFunc),//按时段先后排序
      takeOffPeriod: takeOffPeriodObj.sort(sortFunc)
    }
  };
};

var cache = {};

/* GET users listing. */
router.get('/', function(req, res) {
  // res.send('flights!!!asdf!');
  var param = {
    startCity: req.param('departCityCode') || 'SHA',
    endCity: req.param('arriveCityCode') || 'PEK',
    flyDate: req.param('takeOffTime') || '2014-12-30'
  };

  //不取缓存
  var isRealtime = req.param('isRealtime') || false

  var key = JSON.stringify(param);

  if((key in cache) && !isRealtime){
    console.log('fetch from cache');

    res.send({
      success: true,
      query: req.query,
      flights: cache[key].filghts,
      filter: cache[key].filter
    });
    return;
  }

  //shopping
  shopping.query(param).then(function(flights){
    var info = convert(flights, param.startCity, param.endCity);

    cache[key] = info;

    res.send({
      success: true,
      query: req.query,
      flights: info.filghts,
      filter: info.filter
    });
  }, function (err){
    console.log('查询错误');
    console.log(JSON.stringify(err));
    res.send({
      success: false,
      results: err
    })
  });

});

module.exports = router;
