"use strict";

var soap = require('soap');
var xml2js = require('xml2js');
var _ = require('underscore');
var when = require('when');
var fs = require('fs');
var moment = require('moment');

var url = 'http://59.50.130.66:16881/Shopping.asmx?wsdl';

//配置解析器
var parser = new xml2js.Parser({
  explicitArray: false,
  mergeAttrs: true
});

var formatDate = function (date, tm) {
  var v = tm.split('');
  return date + ' ' + v[0] + v[1] + ':' + v[2] + v[3] + ':00';
};

var formater = function (data, flyDate) {

  //获取航班和舱位信息
  var flights = data.AvJourneys.AvJourney.AvOpt;
  console.log(flights.length);
  //运价信息
  var prices = data.PSn.PS;
  //舱位和运价匹配信息
  var matchs = data.PsAvBinds.PsAvBind;

  //过滤共享航班
  flights = _.filter(flights, function (flight) {
    //过滤共享航班
    return !flight.Flt.codeshare;
  });

  console.log('过滤共享航班后剩余:' + flights.length);

  flights = _.map(flights, function (flight) {
    var fl = flight.Flt;
    //航班标识
    var rph = fl.RPH;
    //舱位列表
    var cabins = fl['class'];

    cabins = _.map(cabins, function (cabin) {
      cabin.flight = {
        airline: fl.airline,
        fltNo: fl.fltNo
      };

      var name = cabin.subClass || cabin.name;
      
      //根据 name，从 matchs 里获取 运价信息标示 seq
      var seq;

      for(var i = 0, size = matchs.length; i < size; i++) {
        var match = matchs[i];
        if(name === match.bkClass && rph === match.avRPH) {
          seq = match.seq;
          cabin.match = match;
          
          break;
        }
      }

      //根据上一步获取的 seq，获取运价
      for(var j = 0, size = prices.length; j < size; j++) {
        var price = prices[j];
        if(seq === price.seq) {
          cabin.priceInfo = price;
          break;
        }
      }

      return {
        CabinName: name,
        Price: cabin.priceInfo ? parseInt(cabin.priceInfo.disAmt) : Infinity,
        LeftCount: ((cabin.av === 'A') ? '' : (cabin.av?cabin.av:0))
      };

    });

    return {
      airline: fl.airline,
      flightNo: fl.airline + fl.fltNo,

      takeOffTime: formatDate(flyDate, fl.deptm),
      departPortCode: fl.dep,
      departPortBuilding: (fl.term && fl.term.dep) ? fl.term.arr : '',

      arriveTime: formatDate(flyDate, fl.arrtm),
      arrivePortCode: fl.arr,
      arrivePortBuilding: (fl.term && fl.term.arr) ? fl.term.arr : '',

      planeType: fl.dev,
      meal: fl.meal,
      stop: fl.stop,
      cabins: cabins
    };
  });

  // console.log(JSON.stringify(flights, null, 2));

  return flights;

  /*var cabins = _.map(flights, function (flight) {
    return flight.Flt['class'];
  });
  
  var results =  _.flatten(cabins);

  if(bool){
    return _.map(results, function (record) {
      return {
        fltNo: record.flight.airline + record.flight.fltNo,
        CabinName: record.subClass || record.name,
        Price: record.priceInfo ? record.priceInfo.disAmt : -1,
        LeftCount: '>' + ((record.av === 'A') ? '9' : record.av)
      };
    });
  }else{
    return results;
  }*/
  
};

var query = function (obj) {
  var deferred = when.defer();
  var param = genParam(obj);

  // var fileName = obj.startCity + obj.endCity + '-' + obj.flyDate;

  soap.createClient(url, function(err, client) {

    if(err) {
      deferred.reject(err);
      return deferred.promise;
    };

    if(!client.AV) {
      deferred.reject({msg: '连接失败!'});
      return deferred.promise;
    }

    client.AV(param, function(err, result) {
      if(err) {
        deferred.reject(err);
      }

      if(!result.AVResult){
        deferred.reject({msg: '请求错误, result.AVResult:' + result.AVResult});
      }

      // fs.writeFile(fileName + '.xml', result.AVResult);

      parser.parseString(result.AVResult, function (err, data) {
        // fs.writeFile(fileName + '-1.json', JSON.stringify(data, null, 2));

        if(err) {
          deferred.reject(err);
        }

        if(data && data.FareInterface && data.FareInterface.Output && data.FareInterface.Output.Result){
          var result = data.FareInterface.Output.Result;
          var error = result.Error;

          if(error){
            deferred.reject(error);
            console.log(error);
          }else{
            var flightResults = result.FlightShopResult;

            var flts = formater(flightResults, obj.flyDate);
            deferred.resolve(flts);

            // fs.writeFile(fileName + '-2.json', JSON.stringify(cabins, null, 2));
          }
        }
      });
    });
  });

  return deferred.promise;
};

var paramTpl =  '<FareInterface>' +
    '<Input>' +
        '<HeaderIn>' +
            '<sysCode>CRS</sysCode>' +
            '<channelID>1E</channelID>' +
            '<channelType>COMMON</channelType>' +
            '<Agency>' +
                '<officeId>#officeId#</officeId>' +
                '<pid>11996</pid>' +
                '<city>BJS</city>' +
            '</Agency>' +
            '<language>CN</language>' +
            '<commandType>FLS</commandType>' +
        '</HeaderIn>' +
        '<Request>' +
            '<FlightShopRequest>' +
                '<OriginDestinationInfo>' +
                    '<ori><%= startCity %></ori>' +
                    '<des><%=endCity%></des>' +
                    '<DepartureDate><%=flyDate%></DepartureDate>' +
                '</OriginDestinationInfo>' +
                '<TravelPreferences>' +
                    // '<cabinClass>Y</cabinClass>' +
                    '<displayCurrCode>CNY</displayCurrCode>' +
                    '<currCode>CNY</currCode>' +
                    '<isDirectFlightOnly>true</isDirectFlightOnly>' +
                    // '<journeyType>OW</journeyType>' +
                    // '<isDealModel>false</isDealModel>' +
                    '<passenger>' +
                        '<number>1</number>' +
                    //     '<type>AD</type>' +
                    '</passenger>' +
                '</TravelPreferences>' +
                '<Option>' +
                    '<fcFeature>0</fcFeature>' +
                    '<isAvNeeded>Y</isAvNeeded>' +
                    '<isPSnNeeded>Y</isPSnNeeded>' +
                    '<isPsAvBindsNeeded>Y</isPsAvBindsNeeded>' +
                    '<isFaresNeeded>Y</isFaresNeeded>' +
                    '<ruleTypeNeeded>Y</ruleTypeNeeded>' +
                    '<format>NOR</format>' +
                    '<lowestOrAll>A</lowestOrAll>' +
                    '<fareSource>ALL</fareSource>' +
                '</Option>' +
            '</FlightShopRequest>' +
        '</Request>' +
    '</Input>' +
'</FareInterface>';

var genParam = function (data) {
  var obj = _.extend({}, data);
  obj.flyDate = moment(new Date(obj.flyDate)).format('DDMMMYY').toUpperCase();
  var result =  {
    xml: _.template(paramTpl)(obj)
  };
  return result;
};

var param = {
  startCity: 'NKG',
  endCity: 'XMN',
  flyDate: '2014-12-24'
};

query(param);

exports.query = query;
