"use strict";

var express = require('express');
var router = express.Router();
var models  = require('../models');

router.get('/', function(req, res) {
  var p = parseInt(req.query.page);
  var l = parseInt(req.query.limit);

  var params = {};

  if(p && l){
    params.offset = p * l;
    params.limit = l;
  }

  models.Permission.findAndCountAll(params).success(function(data) {
    res.send({
      success: true,
      results: data.rows,
      total: data.count
    });
  });

});

module.exports = router;
