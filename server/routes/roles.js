"use strict";

var express = require('express');
var router = express.Router();

var data = {
  success: true,
  results: [{
    roleId: 1,
    roleName: '管理员'
  },{
    roleId: 2,
    roleName: '出差人'
  },{
    roleId: 3,
    roleName: '订票人'
  },{
    roleId: 4,
    roleName: '审核人'
  }]
};

/* GET roles listing. */
router.get('/', function(req, res) {
  res.send(data);
});

module.exports = router;