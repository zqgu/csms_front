"use strict";

var express = require('express');
var router = express.Router();
var models  = require('../models');

/* GET users listing. */
router.get('/', function(req, res) {
  var p = parseInt(req.query.page);
  var l = parseInt(req.query.limit);

  console.log(p,l);

  models.Country.findAndCountAll({
    offset: p * l,
    limit: l
  }).success(function(data) {
    res.send({
      success: 'Express',
      results: data.rows,
      total: data.count
    });
  });
});

router.post('/login', function(req, res) {
  var username = req.param('username');
  var password = req.param('password');

  if(username === 'admin' && password === 'admin'){
    res.send({
      success: true,
      token: new Date().getTime(),
      results: {
        username: 'admin'
      }
    });
  }else{
    res.send({
      success: false,
      msg: '用户名或密码错误!'
    });
  }
});

router.post('/logout', function(req, res) {
  res.send({
    success: true
  });
});

module.exports = router;
