"use strict";

var express = require('express');
var router = express.Router();

var data = {
  "success": true,
  "total": 20,
  "results": [{
    "deptId": "1",
    "parentDeptId": "0",
    "deptName": "部门一",
    "manager": {
      "userId": "1",
      "username": "张无忌"
    }
  },{
    "deptId": "2",
    "parentDeptId": "0",
    "deptName": "部门二",
    "manager": {
      "userId": "2",
      "username": "赵敏"
    }
  }]  
};

router.get('/', function(req, res) {
  res.send(data);
});

router.get('/:id', function(req, res) {
  var id = parseInt(req.params.id);
  res.send(data);
});

module.exports = router;
